#include "test_system.h"

#include <stdio.h>
#include <string.h>

uint8_t testSystem_stdoutIoLoad(uint16_t address, bool isIoReq, void* userData) {
    (void)address;
    (void)isIoReq;
    (void)userData;
    return 0x00;
}

void testSystem_stdoutIoStore(uint16_t address, bool isIoReq, uint8_t value, void* userData) {
    (void)address;
    (void)isIoReq;
    (void)userData;
    putchar(value);
}

struct TestSystemIODev g_testSystem_stdoutDev = {
    testSystem_stdoutIoLoad,
    testSystem_stdoutIoStore,
};

uint8_t testSystem_Load(uint16_t address, bool isIoReq, void* userData) {
    struct TestSystem* ts = (struct TestSystem *)userData;
    if (false == isIoReq) {
        return ts->mem[address];
    } else {
        uint16_t ioAddr = address & 0xff;
        if (NULL != ts->ioDevices[ioAddr]) {
            return ts->ioDevices[ioAddr]->ioLoad(address, true, userData);
        }
    }
    return 0x00;
}

void testSystem_Store(uint16_t address, bool isIoReq, uint8_t value, void* userData) {
    struct TestSystem* ts = (struct TestSystem *)userData;

    if (false == isIoReq) {
        ts->mem[address] = value;
    } else {
        uint16_t ioAddr = address & 0xff;
        if (NULL != ts->ioDevices[ioAddr]) {
            ts->ioDevices[ioAddr]->ioStore(address, true, value, userData);
        }
    }
}

int testSystem_init(struct TestSystem* ts) {
    memset(ts, 0x00, sizeof(struct TestSystem));
    ts->ioDevices[0x00] = &g_testSystem_stdoutDev;
    return 0;
}

int testSystem_loadBinary(struct TestSystem* ts, const char* path, size_t offset) {
    FILE* f = fopen(path, "r");
    if (NULL == f) {
        return -1;
    }
    fread(ts->mem + offset, 1, sizeof(ts->mem) - offset, f);
    fclose(f);
    return 0;
}
