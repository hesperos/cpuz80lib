#ifndef TEST_SYSTEM_H_
#define TEST_SYSTEM_H_

#include <cpuz80/cpuz80.h>

#include <stddef.h>
#include <stdint.h>

struct TestSystemIODev {
    cpuz80LoadT ioLoad;
    cpuz80StoreT ioStore;
};

struct TestSystem {
    uint8_t mem[0x10000];
    struct TestSystemIODev* ioDevices[0x100];
};

uint8_t testSystem_Load(uint16_t address, bool isIoReq, void* userData);

void testSystem_Store(uint16_t address, bool isIoReq, uint8_t value, void* userData);

int testSystem_init(struct TestSystem* ts);

int testSystem_loadBinary(struct TestSystem* ts, const char* path, size_t offset);

#endif /* TEST_SYSTEM_H_ */
