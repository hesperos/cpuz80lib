enable_testing()

set(DATA_BIN_PATH ${PROJECT_BINARY_DIR}/tests/data/bin)
configure_file(data/include/data/data.h.in
    ${PROJECT_BINARY_DIR}/tests/data/include/data/data.h
)

file(COPY data DESTINATION ${PROJECT_BINARY_DIR}/tests)

include_directories(
    ${PROJECT_BINARY_DIR}/3rd/check
    ${PROJECT_BINARY_DIR}/3rd/check/src
    ${PROJECT_BINARY_DIR}/tests/data/include
)

link_directories(
    ${PROJECT_BINARY_DIR}/3rd/check/src
)

add_executable(yaze_tests
    yaze_tests.c
    test_system.c
    tee.c
)

target_link_libraries(yaze_tests
    cpuz80
    pthread
    check
)
add_test(yaze_tests yaze_tests)
