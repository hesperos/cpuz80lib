#include "tee.h"

#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

struct Tee {
    pthread_t t;
    FILE* teeIn;
    FILE* teeOut[2];
    int pipeFd[2];
    struct timeval timeout;
};

static void* tee_thread(void* data) {
    struct Tee* t = (struct Tee *)data;
    fd_set fds;
    int selectFds[] = {
        t->pipeFd[0],
    };
    const size_t nFds = sizeof(selectFds)/sizeof(selectFds[0]);
    uint8_t isRunning = 1;

    // configure non-blocking io on fd
    int flags = fcntl(t->pipeFd[0], F_GETFL, 0);
    fcntl(t->pipeFd[0], F_SETFL, flags | O_NONBLOCK);

    while (isRunning) {
        // reset timeout
        struct timeval timeout = t->timeout;

        // build fd set
        FD_ZERO(&fds);
        for (size_t i = 0; i < nFds; ++i) {
            FD_SET(selectFds[i], &fds);
        }

        // wait for data
        int nReady = select(t->pipeFd[0] + 1, &fds, NULL, NULL, &timeout);

        if (nReady < 0) {
            // most likely the input fd has been closed
            isRunning = 0;
            break;
        }

        // no data ready on any fd
        if (nReady == 0) {
            continue;
        }

        for (size_t i = 0; i < nFds; ++i) {
            if (!FD_ISSET(selectFds[i], &fds)) {
                continue;
            }

            char buf[0x100] = {0x00};
            const int n = read(selectFds[i], buf, sizeof(buf) - 1);
            if (-1 == n) {
                isRunning = 0;
                break;
            }

            const size_t nOuts = sizeof(t->teeOut)/sizeof(t->teeOut[0]);
            for (size_t i = 0; i < nOuts; ++i) {
                fwrite(buf, 1, n, t->teeOut[i]);
                fflush(t->teeOut[i]);
            }
        }
    }

    // close input fds
    for (size_t i = 0; i < nFds; ++i) {
        close(selectFds[i]);
    }
    return NULL;
}

struct Tee* teeCreate(FILE* a, FILE* b)
{
    struct Tee* t = malloc(sizeof(struct Tee));
    if (NULL == t) {
        return NULL;
    }

    t->timeout.tv_sec = 1;
    t->timeout.tv_usec = 0;

    pipe(t->pipeFd);
    if (NULL == (t->teeIn = fdopen(t->pipeFd[1], "w"))) {
        return NULL;
    }

    t->teeOut[0] = a;
    t->teeOut[1] = b;
    pthread_create(&t->t, NULL, tee_thread, t);
    return t;
}

FILE* teeGetFile(struct Tee* t) {
    return t->teeIn;
}

void teeDestroy(struct Tee* t) {
    fclose(t->teeIn);
    close(t->pipeFd[0]);

    pthread_join(t->t, NULL);
    free(t);
}
