#include <check.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#include <errno.h>

#include "test_system.h"
#include "tee.h"

#include <data/data.h>

#include <cpuz80/cpuz80.h>
#include <cpuz80/cpuz80_debug.h>

static void joinPath(char* buffer,
        size_t buffSize,
        const char* dirName,
        const char* baseName) {
    snprintf(buffer, buffSize, "%s/%s", dirName, baseName);
}

static void createBinFilePath(char* buffer,
        size_t buffSize,
        const char* binaryName) {
    joinPath(buffer, buffSize, dataBinPath, binaryName);
}

struct BinInfo {
    const char* bin;
    uint16_t offset;
    uint16_t pc;
    uint64_t nCycles;
};

START_TEST(test_cpu_simple_executables)
{
    struct BinInfo binaries[] = {
        { "tw.bin", 0x00, 0x00, 256 },
    };

    for (size_t bi = 0; bi < sizeof(binaries)/sizeof(struct BinInfo); ++bi) {
        struct cpuz80 cpu;
        struct TestSystem ts;
        char filePath[128] = {0x00};

        createBinFilePath(filePath, sizeof(filePath), binaries[bi].bin);
        testSystem_init(&ts);
        testSystem_loadBinary(&ts, filePath, binaries[bi].offset);
        cpuz80_init(&cpu, testSystem_Load, testSystem_Store, &ts);
        cpuz80_reset(&cpu);

        cpuz80_step(&cpu, binaries[bi].nCycles);
    }

}
END_TEST

START_TEST(test_cpu_zexdoc_cpm_testsuite)
{
    struct BinInfo binaries[] = {
        { "zexdoc.com", 0x0100, 0x0100, 5781000000 },
    };

    for (size_t bi = 0; bi < sizeof(binaries)/sizeof(struct BinInfo); ++bi) {
        struct cpuz80 cpu;
        struct TestSystem ts;
        char filePath[128] = {0x00};

        createBinFilePath(filePath, sizeof(filePath), binaries[bi].bin);
        testSystem_init(&ts);
        testSystem_loadBinary(&ts, filePath, binaries[bi].offset);

        cpuz80_init(&cpu, testSystem_Load, testSystem_Store, &ts);
        cpuz80_reset(&cpu);

        cpu.pc = binaries[bi].pc;
        cpu.sp = 0xf000;

        // should be more than enough for zexdoc/zexall output
        const size_t s = 8 * 1024 * 1024;
        char* buf = malloc(s);
        ck_assert_ptr_nonnull(buf);
        memset(buf, 0x00, s);

        FILE* memStdout = fmemopen(buf, s, "w");
        ck_assert_ptr_nonnull(memStdout);

        struct Tee* tee = teeCreate(memStdout, stdout);
        ck_assert_ptr_nonnull(tee);

        for (uint64_t i = 0; i < binaries[bi].nCycles; i++) {
            cpuz80_step(&cpu, 1);
            cpuz80_cpm_bdos(&cpu, teeGetFile(tee));
        }

        // cleanup
        teeDestroy(tee);
        fclose(memStdout);

        // inspect output
        ck_assert_ptr_nonnull(strstr(buf, "Tests complete"));
        ck_assert_ptr_null(strstr(buf, "ERROR:"));

        // release memory
        free(buf);
    }

}

START_TEST(test_cpu_zexall_cpm_testsuite)
{
    struct BinInfo binaries[] = {
        { "zexall.com", 0x0100, 0x0100, 6000000000 },
    };

    for (size_t bi = 0; bi < sizeof(binaries)/sizeof(struct BinInfo); ++bi) {
        struct cpuz80 cpu;
        struct TestSystem ts;
        char filePath[128] = {0x00};

        createBinFilePath(filePath, sizeof(filePath), binaries[bi].bin);
        testSystem_init(&ts);
        testSystem_loadBinary(&ts, filePath, binaries[bi].offset);

        cpuz80_init(&cpu, testSystem_Load, testSystem_Store, &ts);
        cpuz80_reset(&cpu);

        cpu.pc = binaries[bi].pc;
        cpu.sp = 0xf000;

        // should be more than enough for zexdoc/zexall output
        const size_t s = 8 * 1024 * 1024;
        char* buf = malloc(s);
        ck_assert_ptr_nonnull(buf);
        memset(buf, 0x00, s);

        FILE* memStdout = fmemopen(buf, s, "w");
        ck_assert_ptr_nonnull(memStdout);

        struct Tee* tee = teeCreate(memStdout, stdout);
        ck_assert_ptr_nonnull(tee);

        for (uint64_t i = 0; i < binaries[bi].nCycles; i++) {
            cpuz80_step(&cpu, 1);
            cpuz80_cpm_bdos(&cpu, teeGetFile(tee));
        }

        // cleanup
        teeDestroy(tee);
        fclose(memStdout);

        // inspect output
        ck_assert_ptr_nonnull(strstr(buf, "Tests complete"));
        ck_assert_ptr_null(strstr(buf, "ERROR:"));

        // release memory
        free(buf);
    }

}
END_TEST

Suite* yaze_suite(void) {
    Suite* s;
    TCase *tc_core;

    s = suite_create("yaze");
    tc_core = tcase_create("Core");

    // an hour
    tcase_set_timeout(tc_core, 60 * 60);

    tcase_add_test(tc_core, test_cpu_simple_executables);
    tcase_add_test(tc_core, test_cpu_zexdoc_cpm_testsuite);
    tcase_add_test(tc_core, test_cpu_zexall_cpm_testsuite);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(int argc, const char *argv[])
{
    int number_failed = 0;
    Suite* s = NULL;
    SRunner* sr = NULL;

    s = yaze_suite();
    sr = srunner_create(s);
    srunner_set_fork_status (sr, CK_NOFORK);
    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
