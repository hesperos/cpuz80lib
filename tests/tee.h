#ifndef TEE_H_
#define TEE_H_

#include <stdio.h>

struct Tee;

struct Tee* teeCreate(FILE* a, FILE* b);

FILE* teeGetFile(struct Tee* t);

void teeDestroy(struct Tee* t);

#endif /* TEE_H_ */
