#ifndef CPUZ80_DEBUG_H_
#define CPUZ80_DEBUG_H_

#include "cpuz80_types.h"

#include <stdio.h>

void cpuz80_debug(const char* format, ...);
void cpuz80_dump(struct cpuz80* cpu);
void cpuz80_cpm_bdos(struct cpuz80* cpu, FILE* os);

#endif /* CPUZ80_DEBUG_H_ */
