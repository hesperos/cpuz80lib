#ifndef CPUZ80_UTIL_H_
#define CPUZ80_UTIL_H_

#include "cpuz80_types.h"

#include <stdbool.h>

/**
 * @brief Increment clock cycles counter by n
 *
 * @param cpu
 * @param m number of machine cycles
 * @param n number of ticks
 */
void cpuz80_tick(struct cpuz80* cpu, uint8_t m, uint8_t t);

/**
 * @brief Seeds the CPU pipeline with a new instruction from memory
 *
 * @param cpu
 */
void cpuz80_decoderFetchInstruction(struct cpuz80* cpu);

/**
 * @brief Feeds next instruction byte to the decoder and treats it as a prefix
 * byte
 *
 * @param d decoder
 */
void cpuz80_decoderBuildPrefix(struct cpuz80InstructionDecoder* d);

/**
 * @brief Resets decoder's internal state
 *
 * @param dec
 */
void cpuz80_decoderReset(struct cpuz80InstructionDecoder* dec);

/**
 * @brief Construct a 16-bit word out of two 8 bit values
 *
 * @param high
 * @param low
 *
 * @return 16-bit word
 */
uint16_t cpuz80_makeWord(uint8_t high, uint8_t low);

/**
 * @brief Convert register pair to 16-bit word
 *
 * @param pair
 *
 * @return 16-bit word
 */
uint16_t cpuz80_registerPairToWord(struct cpuz80RegisterPair* pair);

/**
 * @brief Store given 16-bit word into register pair
 *
 * @param pair
 * @param word
 */
void cpuz80_wordToRegisterPair(struct cpuz80RegisterPair* pair, uint16_t word);

/**
 * @brief Return register pair's value and increment it afterwards
 *
 * @param pair
 *
 * @return Non incremented value
 */
uint16_t cpuz80_registerPairPostIncrement(struct cpuz80RegisterPair* pair);

/**
 * @brief Return register pair's value and decrement it afterwards
 *
 * @param pair
 *
 * @return Non decremented value
 */
uint16_t cpuz80_registerPairPostDecrement(struct cpuz80RegisterPair* pair);

/**
 * @brief Increment register pair's value and return in
 *
 * @param pair
 *
 * @return Incremented value
 */
uint16_t cpuz80_registerPairPreIncrement(struct cpuz80RegisterPair* pair);

/**
 * @brief Decrement register pair's value and return it
 *
 * @param pair
 *
 * @return Decremented value
 */
uint16_t cpuz80_registerPairPreDecrement(struct cpuz80RegisterPair* pair);

/**
 * @brief Return register's value and increment it afterwards
 *
 * @param register index
 *
 * @return Non incremented value
 */
uint8_t cpuz80_registerPostIncrement(struct cpuz80* cpu, uint8_t ri);

/**
 * @brief Return register's value and decrement it afterwards
 *
 * @param register index
 *
 * @return Non decremented value
 */
uint8_t cpuz80_registerPostDecrement(struct cpuz80* cpu, uint8_t ri);

/**
 * @brief Return register's value and increment it afterwards
 *
 * @param register index
 *
 * @return Non incremented value
 */
uint8_t cpuz80_registerPostIncrement2(struct cpuz80* cpu, uint8_t ri);

/**
 * @brief Return register's value and decrement it afterwards
 *
 * @param register index
 *
 * @return Non decremented value
 */
uint8_t cpuz80_registerPostDecrement2(struct cpuz80* cpu, uint8_t ri);

/**
 * @brief Return register's value and increment it afterwards
 *
 * @param register index
 *
 * @return Non incremented value
 */
uint8_t cpuz80_registerPostIncrement3(struct cpuz80* cpu, uint8_t ri);

/**
 * @brief Return register's value and decrement it afterwards
 *
 * @param register index
 *
 * @return Non decremented value
 */
uint8_t cpuz80_registerPostDecrement3(struct cpuz80* cpu, uint8_t ri);

/**
 * @brief Increment register's value and return incremented value
 *
 * @param register index
 *
 * @return Incremented value
 */
uint8_t cpuz80_registerPreIncrement(struct cpuz80* cpu, uint8_t ri);

/**
 * @brief Decrement register's value and return decremented value
 *
 * @param register index
 *
 * @return Decremented value
 */
uint8_t cpuz80_registerPreDecrement(struct cpuz80* cpu, uint8_t ri);

/**
 * @brief Get immediate 8-bit address
 *
 * @param cpu
 *
 * @return Immediate 8-bit address
 */
uint8_t cpuz80_get_imm8(struct cpuz80* cpu);

/**
 * @brief Get immediate 16-bit address
 *
 * @param cpu
 *
 * @return Immediate 16-bit address
 */
uint16_t cpuz80_get_imm16(struct cpuz80* cpu);

/**
 * @brief Get IX + d address
 *
 * @param cpu
 *
 * @return IX + d address
 */
uint16_t cpuz80_get_ixd(struct cpuz80* cpu);

/**
 * @brief Get IY + d address
 *
 * @param cpu
 *
 * @return IY + d address
 */
uint16_t cpuz80_get_iyd(struct cpuz80* cpu);

/**
 * @brief Get 16-bit value indirectly
 *
 * @param cpu cpu instance
 *
 * @return (nn) << 8 | (nn + 1)
 */
uint16_t cpuz80_getValue_ind16(struct cpuz80* cpu);

/**
 * @brief Get 8-bit value indirectly
 *
 * @param cpu cpu instance
 *
 * @return (IX+d)
 */
uint8_t cpuz80_getValue_ixd(struct cpuz80* cpu);

/**
 * @brief Get 8-bit value indirectly
 *
 * @param cpu cpu instance
 *
 * @return (IY+d)
 */
uint8_t cpuz80_getValue_iyd(struct cpuz80* cpu);

/**
 * @brief Set 16-bit value indirectly
 *
 * @param cpu cpu instance
 *
 * @return (nn) << 8 | (nn + 1) = value
 */
void cpuz80_setValue_ind16(struct cpuz80* cpu, uint16_t value);

/**
 * @brief Set 16-bit value indirectly to the contents of register pair
 *
 * @param cpu cpu instance
 *
 * @return (nn) << 8 | (nn + 1) = pair
 */
void cpuz80_setValueRp_ind16(struct cpuz80* cpu,
        struct cpuz80RegisterPair* pair);

/**
 * @brief Set 8-bit value indirectly
 *
 * @param cpu cpu instance
 *
 * @return (IX+d) = value
 */
void cpuz80_setValue_ixd(struct cpuz80* cpu, uint8_t value);

/**
 * @brief Set 8-bit value indirectly
 *
 * @param cpu cpu instance
 *
 * @return (IY+d) = value
 */
void cpuz80_setValue_iyd(struct cpuz80* cpu, uint8_t value);

/**
 * @brief Set value of the 8-bit register
 *
 * @param cpu cpu instance
 * @param ri register index
 * @param value value
 */
void cpuz80_setValue_register(struct cpuz80* cpu, uint8_t ri, uint8_t value);

/**
 * @brief Get value of the 8-bit register
 *
 * @param cpu cpu instance
 * @param ri register index
 *
 * @return value value
 */
uint8_t cpuz80_getValue_register(struct cpuz80* cpu, uint8_t ri);

/**
 * @brief Set value of the 8-bit register
 *
 * @param cpu cpu instance
 * @param ri register index
 * @param value value
 */
void cpuz80_setValue_register2(struct cpuz80* cpu, uint8_t ri, uint8_t value);

/**
 * @brief Get value of the 8-bit register
 *
 * @param cpu cpu instance
 * @param ri register index
 *
 * @return value value
 */
uint8_t cpuz80_getValue_register2(struct cpuz80* cpu, uint8_t ri);

/**
 * @brief Set value of the 8-bit register
 *
 * @param cpu cpu instance
 * @param ri register index
 * @param value value
 */
void cpuz80_setValue_register3(struct cpuz80* cpu, uint8_t ri, uint8_t value);

/**
 * @brief Get value of the 8-bit register
 *
 * @param cpu cpu instance
 * @param ri register index
 *
 * @return value value
 */
uint8_t cpuz80_getValue_register3(struct cpuz80* cpu, uint8_t ri);

/**
 * @brief Set value of the 16-bit register pair
 *
 * This API include (HL)
 *
 * @param cpu cpu instance
 * @param ri register pair index
 * @param value value
 */
void cpuz80_setValue_registerPair(struct cpuz80* cpu, uint8_t ri, uint16_t value);

/**
 * @brief Get 16-bit value of selected register pair
 *
 * This API include (HL)
 *
 * @param cpu cpu instance
 * @param ri register pair index
 *
 * @return 16-bit value
 */
uint16_t cpuz80_getValue_registerPair(struct cpuz80* cpu, uint8_t ri);

/**
 * @brief Set value of the 16-bit register pair
 *
 * This API includes SP
 *
 * @param cpu cpu instance
 * @param ri register pair index
 * @param value value
 */
void cpuz80_setValue_registerPair2(struct cpuz80* cpu, uint8_t ri, uint16_t value);

/**
 * @brief Get 16-bit value of selected register pair
 *
 * This API include SP
 *
 * @param cpu cpu instance
 * @param ri register pair index
 *
 * @return 16-bit value
 */
uint16_t cpuz80_getValue_registerPair2(struct cpuz80* cpu, uint8_t ri);

/**
 * @brief Get 16-bit value of selected register pair
 *
 * This API includes IX, IY instead of HL for some prefixes
 *
 * @param cpu
 * @param ri
 *
 * @return 16-bit value
 */
uint16_t cpuz80_getValue_registerPair3(struct cpuz80* cpu, uint8_t ri);

/**
 * @brief Set value of the 16-bit register pair
 *
 * This API includes IX, IY instead of HL for some prefixes
 *
 * @param cpu cpu instance
 * @param ri register pair index
 * @param value value
 */
void cpuz80_setValue_registerPair3(struct cpuz80* cpu, uint8_t ri, uint16_t value);


/**
 * @brief Returns the value for the selected condition
 *
 * @param cpu
 * @param fi flags index
 *
 * @return condition value
 */
bool cpuz80_getValue_flagCondition(struct cpuz80* cpu, uint8_t fi);

/**
* @brief push a value to the stack
*
* @param cpu
* @param value
*/
void cpuz80_push(struct cpuz80* cpu, uint8_t value);

/**
* @brief pop a value from the stack
*
* @param cpu
* @param value
*/
uint8_t cpuz80_pop(struct cpuz80* cpu);

/**
* @brief push a 16-bit word to the stack
*
* @param cpu
* @param value
*/
void cpuz80_push16(struct cpuz80* cpu, uint16_t value);

/**
* @brief pop a 16-bit word from the stack
*
* @param cpu
* @param value
*/
uint16_t cpuz80_pop16(struct cpuz80* cpu);

#endif /* CPUZ80_UTIL_H_ */
