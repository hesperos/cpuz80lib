#ifndef CPUZ80_TABLES_H_
#define CPUZ80_TABLES_H_

#include "cpuz80_types.h"

#include <stdint.h>

struct cpuz80Instruction cpuz80_decodeUnprefixed(struct cpuz80* cpu);
struct cpuz80Instruction cpuz80_decodePrefixDd(struct cpuz80* cpu);
struct cpuz80Instruction cpuz80_decodePrefixFd(struct cpuz80* cpu);
struct cpuz80Instruction cpuz80_decodePrefixDdcb(struct cpuz80* cpu);
struct cpuz80Instruction cpuz80_decodePrefixFdcb(struct cpuz80* cpu);
struct cpuz80Instruction cpuz80_decodePrefixCb(struct cpuz80* cpu);
struct cpuz80Instruction cpuz80_decodePrefixEd(struct cpuz80* cpu);

#endif /* CPUZ80_TABLES_H_ */
