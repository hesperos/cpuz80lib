#ifndef CPUZ80_TYPES_H_
#define CPUZ80_TYPES_H_

#include <stdbool.h>
#include <stdint.h>

/**
 * @brief Enumerates Z80 register banks
 */
enum cpuz80RegisterBankIndex {
    cpuz80RegisterBank0 = 0,
    cpuz80RegisterBank1,

    /// maximum number of banks
    cpuz80RegisterNumberOfBanks,
};

/**
 * @brief Describes a 16-bit pair of 8-bit registers
 */
struct cpuz80RegisterPair {
    uint8_t high;
    uint8_t low;
};

/**
 * @brief Describes a single bank for AF registers pair
 */
struct cpuz80RegisterAFBank {
    struct cpuz80RegisterPair af;
};

/**
 * @brief Describes a bank for the remaining 8-bit Z80 register pairs
 */
struct cpuz80RegisterBank {
    struct cpuz80RegisterPair bc;
    struct cpuz80RegisterPair de;
    struct cpuz80RegisterPair hl;
};

// forward declaration
struct cpuz80;

/**
 * @brief Proptotype for the opcode implementation
 *
 * @param CPU execution context
 */
typedef void (*cpuz80OpCodeT)(struct cpuz80*);

/**
 * @brief Declares an opcode handler along with details
 */
struct cpuz80Instruction {
    cpuz80OpCodeT opcode;
    uint8_t mCycles;
    uint8_t tCycles;
};

/**
 * @brief Declares an instruction table for given prefix
 */
struct cpuz80InstructionTable {
    /// the size of the instruction table
    uint16_t size;

    /// offset of the first opcode (i.e. 0x40 for ED set)
    uint8_t offset;

    /// the pointer to the table itself
    const struct cpuz80Instruction* instructions;
};

/**
 * @brief Represents a decoded instruction
 */
union cpuz80OpDecoded {
    struct {
        uint8_t z : 3;
        uint8_t y : 3;
        uint8_t x : 2;
    } xyz;

    struct {
        uint8_t z : 3;
        uint8_t q : 1;
        uint8_t p : 2;
        uint8_t x : 2;
    } xpqz;

    /// the instruction itself
    uint8_t i;
};

/**
 * @brief Aggregates data used by the instruction decoder
 */
struct cpuz80InstructionDecoder {
    /// holds most recently obtained instruction bytes
    uint16_t prefix;

    /// instruction decomposed into logical blocks
    union cpuz80OpDecoded ir;

    /// displacement byte. In case of prefixed instructions it precedes the
    //opcode so, it makes sense to fetch it and decode it here.
    int8_t d;
};

/**
 * @brief Type of function used by the CPU to read data from memory and IO devices
 *
 * @param address
 * @param isIoReq a flag indicating if the load should be done from IO or memory
 * @param userData pointer to user data
 *
 * @return value byte
 */
typedef uint8_t (*cpuz80LoadT)(uint16_t address, bool isIoReq, void* userData);

/**
 * @brief Type of function used by the CPU to write data to memory or IO
 *
 * @param value value to write
 * @param isIoReq a flag indicating if the write should be done to IO or memory
 * @param userData pointer to user data
 */
typedef void (*cpuz80StoreT)(uint16_t address, bool isIoReq, uint8_t value, void* userData);

struct cpuz80 {
    /// AF register banks
    struct cpuz80RegisterAFBank afBank[cpuz80RegisterNumberOfBanks];

    /// remaining 8-bit pairs register banks
    struct cpuz80RegisterBank bank[cpuz80RegisterNumberOfBanks];

    /// active AF bank selector
    enum cpuz80RegisterBankIndex activeAFBank;

    /// active bank selector
    enum cpuz80RegisterBankIndex activeBank;

    /// IR register pair
    struct cpuz80RegisterPair ir;

    /// index register X
    uint16_t ix;

    /// index register Y
    uint16_t iy;

    /// stack pointer
    uint16_t sp;

    /// program counter
    uint16_t pc;

    /// interrupt mode & interrupt enable flip-flops
    struct {
        uint8_t iff2 : 1;
        uint8_t iff1 : 1;

        /// interrupt mode selector
        uint8_t interruptMode : 2;
    };

    /// memory access
    void *userData;
    cpuz80LoadT l;
    cpuz80StoreT s;

    /// machine cycles counter
    uint32_t mCycles;

    /// clock cycles counter
    uint32_t tCycles;

    /// cpu execution state
    struct {
        /// describes the cpu halt state
        uint8_t isHalted : 1;
    };

    /// instruction decoder
    struct cpuz80InstructionDecoder decoder;
};

#endif /* CPUZ80_TYPES_H_ */
