/* Copyright (C)
 * 2019 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
#ifndef CPUZ80_API_H_
#define CPUZ80_API_H_

#include "cpuz80_types.h"

/**
 * @brief Initializes the CPU
 *
 * Sets up the CPU context data structure.
 *
 * @param cpu pointer to CPU context
 * @param load data load API
 * @param store data store API
 * @param userData user data pointer
 */
void cpuz80_init(struct cpuz80* cpu,
        cpuz80LoadT load,
        cpuz80StoreT store,
        void* userData);

/**
 * @brief Resets CPU
 *
 * Resets already initialized cpu.
 *
 * @param cpu pointer to cpu instance
 */
void cpuz80_reset(struct cpuz80* cpu);

/**
 * @brief Execute a single CPU instruction
 *
 * Performs a single step in the CPU pipeline. A step may not necessarily
 * result in instruction execution. It may be an intermediate instruction
 * decoding step. If no instruction execution happened, the CPU cycles
 * counters won't be incremented.
 *
 * @param cpu pointer to cpu instance
 * @param number of clock cycles to execute
 *
 * @return number of clock cycles executed
 */
uint32_t cpuz80_step(struct cpuz80* cpu, uint32_t t);

/**
 * @brief Switch the CPU into the wait state
 *
 * @param cpu pointer to cpu instance
 */
void cpuz80_wait(struct cpuz80* cpu);

/**
 * @brief Trigger non maskable interrupt
 *
 * @param cpu pointer to cpu instance
 */
void cpuz80_nmi(struct cpuz80* cpu);

/**
 * @brief Trigger an interrupt
 *
 * @param cpu pointer to cpu instance
 */
void cpuz80_int(struct cpuz80* cpu);

#endif /* CPUZ80_API_H_ */
