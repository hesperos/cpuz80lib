/* Copyright (C)
 * 2017 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
#ifndef CPUZ80_STATUS_H_
#define CPUZ80_STATUS_H_

#include "cpuz80_bitops.h"

#include <stdint.h>

/// status register bit meanings
#define CPUZ80_STATUS_BIT_C 0
#define CPUZ80_STATUS_BIT_N 1
#define CPUZ80_STATUS_BIT_PV 2
#define CPUZ80_STATUS_BIT_X3 3
#define CPUZ80_STATUS_BIT_H 4
#define CPUZ80_STATUS_BIT_X5 5
#define CPUZ80_STATUS_BIT_Z 6
#define CPUZ80_STATUS_BIT_S 7

// condition is true if ZERO flag is lowered
#define CPUZ80_STATUS_CC_NZ 0
// condition is true if ZERO flag is raised
#define CPUZ80_STATUS_CC_Z 1
// condition is true if CARRY flag is lowered (no carry)
#define CPUZ80_STATUS_CC_NC 2
// condition is true if CARRY flag is raised (carry)
#define CPUZ80_STATUS_CC_C 3
// condition is true if PV flag is lowered (parity is odd)
#define CPUZ80_STATUS_CC_PO 4
// condition is true if PV flag is raised (parity is even)
#define CPUZ80_STATUS_CC_PE 5
// condition is true if SIGN flag is lowered (result is positive)
#define CPUZ80_STATUS_CC_P 6
// condition is true if SIGN flag is raised (result is negative)
#define CPUZ80_STATUS_CC_M 7

/// status register set ops
#define CPUZ80_SET_C(__status) CPUZ80_BIT_SET(__status, CPUZ80_STATUS_BIT_C)
#define CPUZ80_SET_N(__status) CPUZ80_BIT_SET(__status, CPUZ80_STATUS_BIT_N)
#define CPUZ80_SET_PV(__status) CPUZ80_BIT_SET(__status, CPUZ80_STATUS_BIT_PV)
#define CPUZ80_SET_X3(__status) CPUZ80_BIT_SET(__status, CPUZ80_STATUS_BIT_X3)
#define CPUZ80_SET_H(__status) CPUZ80_BIT_SET(__status, CPUZ80_STATUS_BIT_H)
#define CPUZ80_SET_X5(__status) CPUZ80_BIT_SET(__status, CPUZ80_STATUS_BIT_X5)
#define CPUZ80_SET_Z(__status) CPUZ80_BIT_SET(__status, CPUZ80_STATUS_BIT_Z)
#define CPUZ80_SET_S(__status) CPUZ80_BIT_SET(__status, CPUZ80_STATUS_BIT_S)

/// status register get ops
#define CPUZ80_GET_C(__status) CPUZ80_BIT_GET(__status, CPUZ80_STATUS_BIT_C)
#define CPUZ80_GET_N(__status) CPUZ80_BIT_GET(__status, CPUZ80_STATUS_BIT_N)
#define CPUZ80_GET_PV(__status) CPUZ80_BIT_GET(__status, CPUZ80_STATUS_BIT_PV)
#define CPUZ80_GET_X3(__status) CPUZ80_BIT_GET(__status, CPUZ80_STATUS_BIT_X3)
#define CPUZ80_GET_H(__status) CPUZ80_BIT_GET(__status, CPUZ80_STATUS_BIT_H)
#define CPUZ80_GET_X5(__status) CPUZ80_BIT_GET(__status, CPUZ80_STATUS_BIT_X5)
#define CPUZ80_GET_Z(__status) CPUZ80_BIT_GET(__status, CPUZ80_STATUS_BIT_Z)
#define CPUZ80_GET_S(__status) CPUZ80_BIT_GET(__status, CPUZ80_STATUS_BIT_S)

/// status register get ops
#define CPUZ80_TEST_C(__status) CPUZ80_BIT_TEST(__status, CPUZ80_STATUS_BIT_C)
#define CPUZ80_TEST_N(__status) CPUZ80_BIT_TEST(__status, CPUZ80_STATUS_BIT_N)
#define CPUZ80_TEST_PV(__status) CPUZ80_BIT_TEST(__status, CPUZ80_STATUS_BIT_PV)
#define CPUZ80_TEST_X3(__status) CPUZ80_BIT_TEST(__status, CPUZ80_STATUS_BIT_X3)
#define CPUZ80_TEST_H(__status) CPUZ80_BIT_TEST(__status, CPUZ80_STATUS_BIT_H)
#define CPUZ80_TEST_X5(__status) CPUZ80_BIT_TEST(__status, CPUZ80_STATUS_BIT_X5)
#define CPUZ80_TEST_Z(__status) CPUZ80_BIT_TEST(__status, CPUZ80_STATUS_BIT_Z)
#define CPUZ80_TEST_S(__status) CPUZ80_BIT_TEST(__status, CPUZ80_STATUS_BIT_S)

/// status register clear ops
#define CPUZ80_CLEAR_C(__status) CPUZ80_BIT_CLEAR(__status, CPUZ80_STATUS_BIT_C)
#define CPUZ80_CLEAR_N(__status) CPUZ80_BIT_CLEAR(__status, CPUZ80_STATUS_BIT_N)
#define CPUZ80_CLEAR_PV(__status) CPUZ80_BIT_CLEAR(__status, CPUZ80_STATUS_BIT_PV)
#define CPUZ80_CLEAR_X3(__status) CPUZ80_BIT_CLEAR(__status, CPUZ80_STATUS_BIT_X3)
#define CPUZ80_CLEAR_H(__status) CPUZ80_BIT_CLEAR(__status, CPUZ80_STATUS_BIT_H)
#define CPUZ80_CLEAR_X5(__status) CPUZ80_BIT_CLEAR(__status, CPUZ80_STATUS_BIT_X5)
#define CPUZ80_CLEAR_Z(__status) CPUZ80_BIT_CLEAR(__status, CPUZ80_STATUS_BIT_Z)
#define CPUZ80_CLEAR_S(__status) CPUZ80_BIT_CLEAR(__status, CPUZ80_STATUS_BIT_S)

/// assign bit value accordingly to given boolean
#define CPUZ80_ASSIGN_C(__status, __cond) if ((__cond)) { CPUZ80_SET_C(__status); } else { CPUZ80_CLEAR_C(__status); }
#define CPUZ80_ASSIGN_N(__status, __cond) if ((__cond)) { CPUZ80_SET_N(__status); } else { CPUZ80_CLEAR_N(__status); }
#define CPUZ80_ASSIGN_PV(__status, __cond) if ((__cond)) { CPUZ80_SET_PV(__status); } else { CPUZ80_CLEAR_PV(__status); }
#define CPUZ80_ASSIGN_H(__status, __cond) if ((__cond)) { CPUZ80_SET_H(__status); } else { CPUZ80_CLEAR_H(__status); }
#define CPUZ80_ASSIGN_Z(__status, __cond) if ((__cond)) { CPUZ80_SET_Z(__status); } else { CPUZ80_CLEAR_Z(__status); }
#define CPUZ80_ASSIGN_S(__status, __cond) if ((__cond)) { CPUZ80_SET_S(__status); } else { CPUZ80_CLEAR_S(__status); }
#define CPUZ80_ASSIGN_X3(__status, __cond) if ((__cond)) { CPUZ80_SET_X3(__status); } else { CPUZ80_CLEAR_X3(__status); }
#define CPUZ80_ASSIGN_X5(__status, __cond) if ((__cond)) { CPUZ80_SET_X5(__status); } else { CPUZ80_CLEAR_X5(__status); }

// toggle the value of the flag
#define CPUZ80_TOGGLE_C(__status) CPUZ80_BIT_TOGGLE(__status, CPUZ80_STATUS_BIT_C)
#define CPUZ80_TOGGLE_N(__status) CPUZ80_BIT_TOGGLE(__status, CPUZ80_STATUS_BIT_N)
#define CPUZ80_TOGGLE_PV(__status) CPUZ80_BIT_TOGGLE(__status, CPUZ80_STATUS_BIT_PV)
#define CPUZ80_TOGGLE_H(__status) CPUZ80_BIT_TOGGLE(__status, CPUZ80_STATUS_BIT_H)
#define CPUZ80_TOGGLE_Z(__status) CPUZ80_BIT_TOGGLE(__status, CPUZ80_STATUS_BIT_Z)
#define CPUZ80_TOGGLE_S(__status) CPUZ80_BIT_TOGGLE(__status, CPUZ80_STATUS_BIT_S)

#define CPUZ80_CALC_X35(__status, __op8) \
    ((__status) &= ~0x28, (__status) |= ((__op8) & 0x28))

#define CPUZ80_CALC_C(__status, __result) \
    CPUZ80_ASSIGN_C(__status, ((__result) & 0xff00))

#define CPUZ80_CALC_H(__status, __op81, __op82, __result) \
    ((__status) &= ~0x10, (__status) |= (((__op81) ^ (__op82) ^ (__result)) & (0x01 << CPUZ80_STATUS_BIT_H)))

#define CPUZ80_CALC_PV_ADD(__status, __op81, __op82, __result) \
    CPUZ80_ASSIGN_PV(__status, ((0 == (((__op81) ^ (__op82)) & 0x80)) && (((__op81) ^ (__result)) & 0x80)))

#define CPUZ80_CALC_PV_SUB(__status, __op81, __op82, __result) \
    CPUZ80_ASSIGN_PV(__status, ((((__op81) ^ (__op82)) & 0x80) && !(((__op82) ^ (__result)) & 0x80)))

// the data within the __result will be destroyed
#define CPUZ80_CALC_PV_LOGICAL(__status, __result) \
    CPUZ80_ASSIGN_PV(__status, CPUZ80_IS_BYTE_PARITY_EVEN(__result))

#endif /* CPUZ80_STATUS_H_ */
