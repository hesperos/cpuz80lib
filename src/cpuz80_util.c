#include "../include/cpuz80/cpuz80_status.h"
#include "../include/cpuz80/cpuz80_util.h"

/* ========================================================================== */

void cpuz80_push(struct cpuz80* cpu, uint8_t value) {
    cpu->s(--cpu->sp, false, value, cpu->userData);
}

void cpuz80_push16(struct cpuz80* cpu, uint16_t value) {
    cpu->s(--cpu->sp, false, value >> 8, cpu->userData);
    cpu->s(--cpu->sp, false, value & 0xff, cpu->userData);
}

uint8_t cpuz80_pop(struct cpuz80* cpu) {
    return cpu->l(cpu->sp++, false, cpu->userData);
}

uint16_t cpuz80_pop16(struct cpuz80* cpu) {
    const uint8_t low = cpu->l(cpu->sp++, false, cpu->userData);
    return cpuz80_makeWord(cpu->l(cpu->sp++, false, cpu->userData), low);
}

/* ========================================================================== */

void cpuz80_tick(struct cpuz80* cpu, uint8_t m, uint8_t t) {
    cpu->tCycles += t;
    cpu->mCycles += m;
}

void cpuz80_decoderReset(struct cpuz80InstructionDecoder* dec) {
    dec->prefix = 0x00;
    dec->d = 0x00;
    dec->ir.i = 0x00;
}

void cpuz80_decoderBuildPrefix(struct cpuz80InstructionDecoder* dec) {
    dec->prefix <<= 8;
    dec->prefix |= dec->ir.i;
}

void cpuz80_decoderFetchInstruction(struct cpuz80* cpu) {
    cpu->decoder.ir.i = cpuz80_get_imm8(cpu);
}

/* ========================================================================== */

uint16_t cpuz80_makeWord(uint8_t high, uint8_t low) {
    const uint16_t result = high;
    return (result << 8) | low;
}

uint16_t cpuz80_registerPairToWord(struct cpuz80RegisterPair* pair) {
    return cpuz80_makeWord(pair->high, pair->low);
}

void cpuz80_wordToRegisterPair(struct cpuz80RegisterPair* pair,
        uint16_t word) {
    pair->low = (word & 0xff);
    pair->high = (word >> 8);
}

/* ========================================================================== */

inline uint8_t cpuz80_get_imm8(struct cpuz80* cpu) {
    return cpu->l(cpu->pc++, false, cpu->userData);
}

uint16_t cpuz80_get_imm16(struct cpuz80* cpu) {
    const uint8_t low = cpuz80_get_imm8(cpu);
    return cpuz80_makeWord(cpuz80_get_imm8(cpu), low);
}

uint16_t cpuz80_get_ixd(struct cpuz80* cpu) {
    return cpu->ix + cpu->decoder.d;
}

uint16_t cpuz80_get_iyd(struct cpuz80* cpu) {
    return cpu->iy + cpu->decoder.d;
}

/* ========================================================================== */

uint16_t cpuz80_getValue_ind16(struct cpuz80* cpu) {
    const uint16_t ia = cpuz80_get_imm16(cpu);
    return cpuz80_makeWord(cpu->l(ia + 1, false, cpu->userData),
            cpu->l(ia, false, cpu->userData));
}

uint8_t cpuz80_getValue_ixd(struct cpuz80* cpu) {
    const uint16_t addr = cpuz80_get_ixd(cpu);
    return cpu->l(addr, false, cpu->userData);
}

uint8_t cpuz80_getValue_iyd(struct cpuz80* cpu) {
    const uint16_t addr = cpuz80_get_iyd(cpu);
    return cpu->l(addr, false, cpu->userData);
}

void cpuz80_setValue_ind16(struct cpuz80* cpu, uint16_t value) {
    const uint16_t ia = cpuz80_get_imm16(cpu);
    cpu->s(ia, false, value & 0xff, cpu->userData);
    cpu->s(ia + 1, false, value >> 8, cpu->userData);
}

void cpuz80_setValueRp_ind16(struct cpuz80* cpu,
        struct cpuz80RegisterPair* pair) {
    const uint16_t ia = cpuz80_get_imm16(cpu);
    cpu->s(ia, false, pair->low, cpu->userData);
    cpu->s(ia + 1, false, pair->high, cpu->userData);
}

void cpuz80_setValue_ixd(struct cpuz80* cpu, uint8_t value) {
    const uint16_t addr = cpuz80_get_ixd(cpu);
    cpu->s(addr, false, value, cpu->userData);
}

void cpuz80_setValue_iyd(struct cpuz80* cpu, uint8_t value) {
    const uint16_t addr = cpuz80_get_iyd(cpu);
    cpu->s(addr, false, value, cpu->userData);
}

void cpuz80_setValue_register(struct cpuz80* cpu, uint8_t ri, uint8_t value) {
    switch(ri) {
        case 0: cpu->bank[cpu->activeBank].bc.high = value; break;
        case 1: cpu->bank[cpu->activeBank].bc.low = value; break;
        case 2: cpu->bank[cpu->activeBank].de.high = value; break;
        case 3: cpu->bank[cpu->activeBank].de.low = value; break;
        case 4: cpu->bank[cpu->activeBank].hl.high = value; break;
        case 5: cpu->bank[cpu->activeBank].hl.low = value; break;
        case 6: cpu->s(cpuz80_registerPairToWord(
                            &cpu->bank[cpu->activeBank].hl),
                        false,
                        value,
                        cpu->userData);
            break;
        case 7: cpu->afBank[cpu->activeAFBank].af.high = value; break;
    }
}

uint8_t cpuz80_getValue_register(struct cpuz80* cpu, uint8_t ri) {
    switch(ri) {
        case 0: return cpu->bank[cpu->activeBank].bc.high;
        case 1: return cpu->bank[cpu->activeBank].bc.low;
        case 2: return cpu->bank[cpu->activeBank].de.high;
        case 3: return cpu->bank[cpu->activeBank].de.low;
        case 4: return cpu->bank[cpu->activeBank].hl.high;
        case 5: return cpu->bank[cpu->activeBank].hl.low;
        case 6: return cpu->l(cpuz80_registerPairToWord(
                            &cpu->bank[cpu->activeBank].hl),
                        false,
                        cpu->userData);
        case 7: return cpu->afBank[cpu->activeAFBank].af.high;
    }
    return 0x00;
}

void cpuz80_setValue_register2(struct cpuz80* cpu, uint8_t ri, uint8_t value) {
    switch(ri) {
        case 0: cpu->bank[cpu->activeBank].bc.high = value; break;
        case 1: cpu->bank[cpu->activeBank].bc.low = value; break;
        case 2: cpu->bank[cpu->activeBank].de.high = value; break;
        case 3: cpu->bank[cpu->activeBank].de.low = value; break;
        case 4: cpu->ix = (((uint16_t)value) << 8) | (cpu->ix & 0xff); break;
        case 5: cpu->ix = (cpu->ix & 0xff00) | value; break;
        case 6: cpu->s(cpuz80_registerPairToWord(
                            &cpu->bank[cpu->activeBank].hl),
                        false,
                        value,
                        cpu->userData);
            break;
        case 7: cpu->afBank[cpu->activeAFBank].af.high = value; break;
    }
}

uint8_t cpuz80_getValue_register2(struct cpuz80* cpu, uint8_t ri) {
    switch(ri) {
        case 0: return cpu->bank[cpu->activeBank].bc.high;
        case 1: return cpu->bank[cpu->activeBank].bc.low;
        case 2: return cpu->bank[cpu->activeBank].de.high;
        case 3: return cpu->bank[cpu->activeBank].de.low;
        case 4: return (cpu->ix >> 8) & 0xff;
        case 5: return (cpu->ix & 0xff);
        case 6: return cpu->l(cpuz80_registerPairToWord(
                            &cpu->bank[cpu->activeBank].hl),
                        false,
                        cpu->userData);
        case 7: return cpu->afBank[cpu->activeAFBank].af.high;
    }
    return 0x00;
}

void cpuz80_setValue_register3(struct cpuz80* cpu, uint8_t ri, uint8_t value) {
    switch(ri) {
        case 0: cpu->bank[cpu->activeBank].bc.high = value; break;
        case 1: cpu->bank[cpu->activeBank].bc.low = value; break;
        case 2: cpu->bank[cpu->activeBank].de.high = value; break;
        case 3: cpu->bank[cpu->activeBank].de.low = value; break;
        case 4: cpu->iy = (((uint16_t)value) << 8) | (cpu->iy & 0xff); break;
        case 5: cpu->iy = (cpu->iy & 0xff00) | value; break;
        case 6: cpu->s(cpuz80_registerPairToWord(
                            &cpu->bank[cpu->activeBank].hl),
                        false,
                        value,
                        cpu->userData);
            break;
        case 7: cpu->afBank[cpu->activeAFBank].af.high = value; break;
    }
}

uint8_t cpuz80_getValue_register3(struct cpuz80* cpu, uint8_t ri) {
    switch(ri) {
        case 0: return cpu->bank[cpu->activeBank].bc.high;
        case 1: return cpu->bank[cpu->activeBank].bc.low;
        case 2: return cpu->bank[cpu->activeBank].de.high;
        case 3: return cpu->bank[cpu->activeBank].de.low;
        case 4: return (cpu->iy >> 8) & 0xff;
        case 5: return (cpu->iy & 0xff);
        case 6: return cpu->l(cpuz80_registerPairToWord(
                            &cpu->bank[cpu->activeBank].hl),
                        false,
                        cpu->userData);
        case 7: return cpu->afBank[cpu->activeAFBank].af.high;
    }
    return 0x00;
}

void cpuz80_setValue_registerPair(struct cpuz80* cpu, uint8_t ri, uint16_t value) {
    switch(ri) {
        case 0: cpuz80_wordToRegisterPair(&cpu->bank[cpu->activeBank].bc, value);
            break;
        case 1: cpuz80_wordToRegisterPair(&cpu->bank[cpu->activeBank].de, value);
            break;
        case 2: cpuz80_wordToRegisterPair(&cpu->bank[cpu->activeBank].hl, value);
            break;
        case 3: cpu->sp = value;
            break;
    }
}

uint16_t cpuz80_getValue_registerPair(struct cpuz80* cpu, uint8_t ri) {
    switch(ri) {
        case 0: return cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].bc);
        case 1: return cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].de);
        case 2: return cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].hl);
        case 3: return cpu->sp;
    }
    return 0x00;
}

void cpuz80_setValue_registerPair2(struct cpuz80* cpu, uint8_t ri, uint16_t value) {
    switch(ri) {
        case 0: cpuz80_wordToRegisterPair(&cpu->bank[cpu->activeBank].bc, value);
            break;
        case 1: cpuz80_wordToRegisterPair(&cpu->bank[cpu->activeBank].de, value);
            break;
        case 2: cpuz80_wordToRegisterPair(&cpu->bank[cpu->activeBank].hl, value);
            break;
        case 3: cpuz80_wordToRegisterPair(&cpu->afBank[cpu->activeAFBank].af, value);
            break;
    }
}

uint16_t cpuz80_getValue_registerPair2(struct cpuz80* cpu, uint8_t ri) {
    switch(ri) {
        case 0: return cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].bc);
        case 1: return cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].de);
        case 2: return cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].hl);
        case 3: return cpuz80_registerPairToWord(&cpu->afBank[cpu->activeAFBank].af);
    }
    return 0x00;
}

uint16_t cpuz80_getValue_registerPair3(struct cpuz80* cpu, uint8_t ri) {
    switch(ri) {
        case 0:
        case 1:
        case 3:
            return cpuz80_getValue_registerPair(cpu, ri);

        case 2:
            return cpu->decoder.prefix == 0xdd ? cpu->ix :
                (cpu->decoder.prefix == 0xfd ? cpu->iy :
                 cpuz80_getValue_registerPair(cpu, ri));
    }
    return 0x00;
}

void cpuz80_setValue_registerPair3(struct cpuz80* cpu, uint8_t ri, uint16_t value) {
    switch(ri) {
        case 0:
        case 1:
        case 3:
            cpuz80_setValue_registerPair(cpu, ri, value);
            break;

        case 2:
            switch(cpu->decoder.prefix) {
                case 0xdd:
                    cpu->ix = value;
                    break;

                case 0xfd:
                    cpu->iy = value;
                    break;

                case 0x00:
                default:
                    cpuz80_setValue_registerPair(cpu, ri, value);
                    break;
            }
            break;
    }
}

bool cpuz80_getValue_flagCondition(struct cpuz80* cpu, uint8_t fi) {
    switch (fi) {
        case CPUZ80_STATUS_CC_NZ:
            return CPUZ80_TEST_Z(cpu->afBank[cpu->activeAFBank].af.low) == 0;
        case CPUZ80_STATUS_CC_Z:
            return CPUZ80_TEST_Z(cpu->afBank[cpu->activeAFBank].af.low) != 0;
        case CPUZ80_STATUS_CC_NC:
            return CPUZ80_TEST_C(cpu->afBank[cpu->activeAFBank].af.low) == 0;
        case CPUZ80_STATUS_CC_C:
            return CPUZ80_TEST_C(cpu->afBank[cpu->activeAFBank].af.low) != 0;
        case CPUZ80_STATUS_CC_PO:
            return CPUZ80_TEST_PV(cpu->afBank[cpu->activeAFBank].af.low) == 0;
        case CPUZ80_STATUS_CC_PE:
            return CPUZ80_TEST_PV(cpu->afBank[cpu->activeAFBank].af.low) != 0;
        case CPUZ80_STATUS_CC_P:
            return CPUZ80_TEST_S(cpu->afBank[cpu->activeAFBank].af.low) == 0;
        case CPUZ80_STATUS_CC_M:
            return CPUZ80_TEST_S(cpu->afBank[cpu->activeAFBank].af.low) != 0;
    }
    return false;
}

/* ========================================================================== */

uint16_t cpuz80_registerPairPostIncrement(struct cpuz80RegisterPair* pair) {
    uint16_t word = cpuz80_registerPairToWord(pair);
    cpuz80_wordToRegisterPair(pair, word + 1);
    return word;
}

uint16_t cpuz80_registerPairPostDecrement(struct cpuz80RegisterPair* pair) {
    uint16_t word = cpuz80_registerPairToWord(pair);
    cpuz80_wordToRegisterPair(pair, word - 1);
    return word;
}

uint16_t cpuz80_registerPairPreIncrement(struct cpuz80RegisterPair* pair) {
    uint16_t word = cpuz80_registerPairToWord(pair);
    cpuz80_wordToRegisterPair(pair, ++word);
    return word;
}

uint16_t cpuz80_registerPairPreDecrement(struct cpuz80RegisterPair* pair) {
    uint16_t word = cpuz80_registerPairToWord(pair);
    cpuz80_wordToRegisterPair(pair, --word);
    return word;
}

uint8_t cpuz80_registerPostIncrement(struct cpuz80* cpu, uint8_t ri) {
    uint8_t value = cpuz80_getValue_register(cpu, ri);
    cpuz80_setValue_register(cpu, ri, value + 1);
    return value;
}

uint8_t cpuz80_registerPostDecrement(struct cpuz80* cpu, uint8_t ri) {
    uint8_t value = cpuz80_getValue_register(cpu, ri);
    cpuz80_setValue_register(cpu, ri, value - 1);
    return value;
}

uint8_t cpuz80_registerPostIncrement2(struct cpuz80* cpu, uint8_t ri) {
    uint8_t value = cpuz80_getValue_register2(cpu, ri);
    cpuz80_setValue_register2(cpu, ri, value + 1);
    return value;
}

uint8_t cpuz80_registerPostDecrement2(struct cpuz80* cpu, uint8_t ri) {
    uint8_t value = cpuz80_getValue_register2(cpu, ri);
    cpuz80_setValue_register2(cpu, ri, value - 1);
    return value;
}

uint8_t cpuz80_registerPostIncrement3(struct cpuz80* cpu, uint8_t ri) {
    uint8_t value = cpuz80_getValue_register3(cpu, ri);
    cpuz80_setValue_register3(cpu, ri, value + 1);
    return value;
}

uint8_t cpuz80_registerPostDecrement3(struct cpuz80* cpu, uint8_t ri) {
    uint8_t value = cpuz80_getValue_register2(cpu, ri);
    cpuz80_setValue_register3(cpu, ri, value - 1);
    return value;
}

uint8_t cpuz80_registerPreIncrement(struct cpuz80* cpu, uint8_t ri) {
    uint8_t value = cpuz80_getValue_register(cpu, ri);
    cpuz80_setValue_register(cpu, ri, ++value);
    return value;
}

uint8_t cpuz80_registerPreDecrement(struct cpuz80* cpu, uint8_t ri) {
    uint8_t value = cpuz80_getValue_register(cpu, ri);
    cpuz80_setValue_register(cpu, ri, --value);
    return value;
}

/* ========================================================================== */
