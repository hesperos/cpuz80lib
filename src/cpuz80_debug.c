#include "../include/cpuz80/cpuz80_debug.h"
#include "../include/cpuz80/cpuz80_util.h"

#include <stdarg.h>

void cpuz80_debug(const char* format, ...) {
    char buffer[1024] = {0x00};
    va_list args;
    va_start(args, format);
    vsnprintf(buffer, sizeof(buffer), format, args);
    va_end(args);

    fprintf(stderr, "[Z80] --> %s\n", buffer);
}

void cpuz80_dump(struct cpuz80* cpu) {
    fprintf(stderr, "AFBank: %1d, Bank: %1d\n",
            cpu->activeAFBank,
            cpu->activeBank);

    fprintf(stderr, "A: 0x%02x, F: 0x%02x\n",
            cpu->afBank[cpu->activeAFBank].af.high,
            cpu->afBank[cpu->activeAFBank].af.low);

    fprintf(stderr, "B: 0x%02x, C: 0x%02x\n",
            cpu->bank[cpu->activeBank].bc.high,
            cpu->bank[cpu->activeBank].bc.low);

    fprintf(stderr, "D: 0x%02x, E: 0x%02x\n",
            cpu->bank[cpu->activeBank].de.high,
            cpu->bank[cpu->activeBank].de.low);

    fprintf(stderr, "H: 0x%02x, L: 0x%02x\n",
            cpu->bank[cpu->activeBank].hl.high,
            cpu->bank[cpu->activeBank].hl.low);

    fprintf(stderr, "I: 0x%02x, R: 0x%02x\n",
            cpu->ir.high,
            cpu->ir.low);

    fprintf(stderr, "IX: 0x%04x, IY: 0x%04x\n",
            cpu->ix, cpu->iy);

    fprintf(stderr, "PC: 0x%04x, SP: 0x%04x\n",
            cpu->pc, cpu->sp);

    fprintf(stderr, "IFF1/IFF2: %d/%d, IMode: 0x%02x\n",
            cpu->iff1, cpu->iff2, cpu->interruptMode);

    fprintf(stderr, "mCycles: %u, tCycles: %u\n",
            cpu->mCycles, cpu->tCycles);

    fprintf(stderr, "prefix: 0x%04x, instr: 0x%02x\n",
            cpu->decoder.prefix, cpu->decoder.ir.i);

    fprintf(stderr, "---\n");
}

void cpuz80_cpm_bdos(struct cpuz80* cpu, FILE* os) {
    if (cpu->pc == 0x0005) {
        if (cpu->bank[cpu->activeBank].bc.low == 0x02) {
            // print char from register E
            fprintf(os, "%c", cpu->bank[cpu->activeBank].de.low);
        } else if (cpu->bank[cpu->activeBank].bc.low == 0x09) {
            // print bytes from address (DE) until '$'
            uint8_t c = 0;
            uint16_t offset = 0x00;
            const uint16_t base =
                cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].de);
            while ('$' != (c = cpu->l(base + offset++, false, cpu->userData))) {
                fprintf(os, "%c", c);
            }
        }

        cpu->pc = cpuz80_pop16(cpu);
        fflush(os);
    }
}
