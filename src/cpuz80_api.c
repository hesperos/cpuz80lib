#include "../include/cpuz80/cpuz80_api.h"
#include "../include/cpuz80/cpuz80_debug.h"
#include "../include/cpuz80/cpuz80_tables.h"
#include "../include/cpuz80/cpuz80_util.h"

#include <string.h>
#include <stdbool.h>

/* ========================================================================== */

void cpuz80_init(struct cpuz80* cpu,
        cpuz80LoadT load,
        cpuz80StoreT store,
        void* userData) {

    memset(cpu, 0x00, sizeof(struct cpuz80));
    cpu->l = load;
    cpu->s = store;
    cpu->userData = userData;
}

void cpuz80_reset(struct cpuz80* cpu) {
    cpu->sp = 0xffff;
    cpu->ix = 0x0000;
    cpu->iy = 0x0000;
    memset(cpu->afBank, 0x00, sizeof(cpu->afBank));
    memset(cpu->bank, 0x00, sizeof(cpu->bank));
    memset(&cpu->ir, 0x00, sizeof(cpu->ir));
    cpu->pc = 0x0000;
    cpu->iff1 = 0;
    cpu->iff2 = 0;
    cpu->interruptMode = 0;
    cpu->isHalted = 0;
    cpu->decoder.prefix = 0x00;
    cpu->decoder.d = 0x00;
    cpu->mCycles = 0;
    cpu->tCycles = 0;
    cpuz80_tick(cpu, 1, 3);
}

uint32_t cpuz80_step(struct cpuz80* cpu, uint32_t t) {
    uint32_t t0 = cpu->tCycles;
    uint32_t tExecuted = 0;

    while (t > (tExecuted = (cpu->tCycles - t0)) && (cpu->tCycles >= t0)) {

        // feed the pipeline
        cpuz80_decoderFetchInstruction(cpu);

        // incremented after each instruction fetch
        cpu->ir.low = (cpu->ir.low + 1) & 0x7f;

        // lookup opcode
        struct cpuz80Instruction i = cpuz80_decodeUnprefixed(cpu);

        // execute opcode
        i.opcode(cpu);
        cpuz80_tick(cpu, i.mCycles, i.tCycles);
        cpuz80_decoderReset(&cpu->decoder);
    }

    return tExecuted;
}

void cpuz80_wait(struct cpuz80* cpu) {

}

void cpuz80_nmi(struct cpuz80* cpu) {
    cpu->iff2 = cpu->iff1;
    cpu->iff1 = 0x00;
}

void cpuz80_int(struct cpuz80* cpu) {
    if (cpu->iff1) {
        cpu->iff1 = 0x00;
        cpu->iff2 = 0x00;

        switch (cpu->interruptMode) {
            case 0x00:
                break;

            case 0x01:
                cpuz80_push16(cpu, cpu->pc);
                cpu->pc = 0x0038;
                break;

            case 0x02:
                break;
        }
    }
}

/* ========================================================================== */
