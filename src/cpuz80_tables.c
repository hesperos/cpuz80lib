#include "../include/cpuz80/cpuz80_tables.h"
#include "../include/cpuz80/cpuz80_util.h"
#include "../include/cpuz80/cpuz80_status.h"

#include <stdbool.h>
#include <stdio.h>

/* ========================================================================== */

static void cpuz80_nop(struct cpuz80* cpu) {
    /* NOP */
    (void)cpu;
}

static void cpuz80_load8_rr(struct cpuz80* cpu) {
    /* LD r,r' */
    /* LD r,(HL) */
    /* LD (HL),r */
    cpuz80_setValue_register(cpu, cpu->decoder.ir.xyz.y,
            cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.z));
}

static void cpuz80_load8_pp(struct cpuz80* cpu) {
    /* LD p,p' */
    cpuz80_setValue_register2(cpu, cpu->decoder.ir.xyz.y,
            cpuz80_getValue_register2(cpu, cpu->decoder.ir.xyz.z));
}

static void cpuz80_load8_qq(struct cpuz80* cpu) {
    /* LD q,q' */
    cpuz80_setValue_register3(cpu, cpu->decoder.ir.xyz.y,
            cpuz80_getValue_register3(cpu, cpu->decoder.ir.xyz.z));
}

static void cpuz80_load8_rn(struct cpuz80* cpu) {
    /* LD r,n */
    /* LD (HL),n */
    cpuz80_setValue_register(cpu, cpu->decoder.ir.xyz.y, cpuz80_get_imm8(cpu));
}

static void cpuz80_load8_pn(struct cpuz80* cpu) {
    /* LD p,n */
    cpuz80_setValue_register2(cpu, cpu->decoder.ir.xyz.y, cpuz80_get_imm8(cpu));
}

static void cpuz80_load8_qn(struct cpuz80* cpu) {
    /* LD q,n */
    cpuz80_setValue_register3(cpu, cpu->decoder.ir.xyz.y, cpuz80_get_imm8(cpu));
}

static void cpuz80_load8_aindirect(struct cpuz80* cpu) {
    uint8_t value = 0x00;
    switch(cpu->decoder.ir.xyz.y) {
        case 0x01: /* LD A,(BC) */
            value = cpu->l(cpuz80_registerPairToWord(
                        &cpu->bank[cpu->activeBank].bc),
                    false,
                    cpu->userData);
            break;
        case 0x03: /* LD A,(DE) */
            value = cpu->l(cpuz80_registerPairToWord(
                        &cpu->bank[cpu->activeBank].de),
                    false,
                    cpu->userData);
            break;
        case 0x07: /* LD A,(nn) */
            value = cpu->l(cpuz80_get_imm16(cpu), false, cpu->userData);
            break;
    }
    cpu->afBank[cpu->activeAFBank].af.high = value;
}

static void cpuz80_load8_indirecta(struct cpuz80* cpu) {
    uint16_t ia = 0x00;
    switch(cpu->decoder.ir.xyz.y) {
        case 0x00: /* LD (BC),A */
            ia = cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].bc);
            break;
        case 0x02: /* LD (DE),A */
            ia = cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].de);
            break;
        case 0x06: /* LD (nn),A */
            ia = cpuz80_get_imm16(cpu);
            break;
    }
    cpu->s(ia, false, cpu->afBank[cpu->activeAFBank].af.high, cpu->userData);
}

static void cpuz80_load16_rpn(struct cpuz80* cpu) {
    /* LD dd,nn */
    const uint16_t value = cpuz80_get_imm16(cpu);
    cpuz80_setValue_registerPair(cpu, cpu->decoder.ir.xpqz.p, value);
}

static void cpuz80_load16_hln(struct cpuz80* cpu) {
    /* LD HL,(nn) */
    const uint16_t ia = cpuz80_get_imm16(cpu);
    cpu->bank[cpu->activeBank].hl.low = cpu->l(ia, false, cpu->userData);
    cpu->bank[cpu->activeBank].hl.high = cpu->l(ia + 1, false, cpu->userData);
}

static void cpuz80_load16_nhl(struct cpuz80* cpu) {
    /* LD (nn),HL */
    /* LD (nn),dd */
    cpuz80_setValue_ind16(cpu,
            cpuz80_getValue_registerPair(cpu, cpu->decoder.ir.xpqz.p));
}

static void cpuz80_load16_sphl(struct cpuz80* cpu) {
    /* LD SP,HL */
    cpu->sp = cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].hl);
}

static void cpuz80_push_rp2(struct cpuz80* cpu) {
    /* PUSH qq */
    cpuz80_push16(cpu, cpuz80_getValue_registerPair2(cpu, cpu->decoder.ir.xpqz.p));
}

static void cpuz80_pop_rp2(struct cpuz80* cpu) {
    /* POP qq */
    cpuz80_setValue_registerPair2(cpu, cpu->decoder.ir.xpqz.p, cpuz80_pop16(cpu));
}

static void cpuz80_ex_dehl(struct cpuz80* cpu) {
    /* EX DE,HL */
    const struct cpuz80RegisterPair tmp =
        cpu->bank[cpu->activeBank].de;
    cpu->bank[cpu->activeBank].de = cpu->bank[cpu->activeBank].hl;
    cpu->bank[cpu->activeBank].hl = tmp;
}

static void cpuz80_ex_afaf(struct cpuz80* cpu) {
    /* EX AF,AF' */
    cpu->activeAFBank = (cpu->activeAFBank + 1) % cpuz80RegisterNumberOfBanks;
}

static void cpuz80_exx(struct cpuz80* cpu) {
    /* EXX */
    cpu->activeBank = (cpu->activeBank + 1) % cpuz80RegisterNumberOfBanks;
}

static void cpuz80_ex_sphl(struct cpuz80* cpu) {
    /* EX (SP),HL */
    uint8_t tmp = cpu->l(cpu->sp, false, cpu->userData);
    cpu->s(cpu->sp, false, cpu->bank[cpu->activeBank].hl.low, cpu->userData);
    cpu->bank[cpu->activeBank].hl.low = tmp;

    tmp = cpu->l(cpu->sp + 1, false, cpu->userData);
    cpu->s(cpu->sp + 1,
            false,
            cpu->bank[cpu->activeBank].hl.high, cpu->userData);
    cpu->bank[cpu->activeBank].hl.high = tmp;
}

static void cpuz80_load8_rixd(struct cpuz80* cpu) {
    /* LD r,(IX+d) */
    cpu->decoder.d = cpuz80_get_imm8(cpu);
    cpuz80_setValue_register(cpu, cpu->decoder.ir.xyz.y, cpuz80_getValue_ixd(cpu));
}

static void cpuz80_load8_ixdr(struct cpuz80* cpu) {
    /* LD (IX+d),r */
    cpu->decoder.d = cpuz80_get_imm8(cpu);
    cpuz80_setValue_ixd(cpu, cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.z));
}

static void cpuz80_load8_ixdn(struct cpuz80* cpu) {
    /* LD (IX+d),n */
    cpu->decoder.d = cpuz80_get_imm8(cpu);
    cpuz80_setValue_ixd(cpu, cpuz80_get_imm8(cpu));
}

static void cpuz80_load8_riyd(struct cpuz80* cpu) {
    /* LD r,(IY+d) */
    cpu->decoder.d = cpuz80_get_imm8(cpu);
    cpuz80_setValue_register(cpu, cpu->decoder.ir.xyz.y, cpuz80_getValue_iyd(cpu));
}

static void cpuz80_load8_iydr(struct cpuz80* cpu) {
    /* LD (IY+d),r */
    cpu->decoder.d = cpuz80_get_imm8(cpu);
    cpuz80_setValue_iyd(cpu, cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.z));
}

static void cpuz80_load8_iydn(struct cpuz80* cpu) {
    /* LD (IY+d),n */
    cpu->decoder.d = cpuz80_get_imm8(cpu);
    cpuz80_setValue_iyd(cpu, cpuz80_get_imm8(cpu));
}

static void cpuz80_load8_ai(struct cpuz80* cpu) {
    /* LD A,I */
    cpu->afBank[cpu->activeAFBank].af.high = cpu->ir.high;
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, cpu->iff2);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, cpu->ir.high == 0);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, cpu->ir.high & 0x80);
}

static void cpuz80_load8_ar(struct cpuz80* cpu) {
    /* LD A,R */
    cpu->afBank[cpu->activeAFBank].af.high = cpu->ir.low;
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, cpu->iff2);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, cpu->ir.low == 0);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, cpu->ir.low & 0x80);
}

static void cpuz80_load8_ia(struct cpuz80* cpu) {
    /* LD I,A */
    cpu->ir.high = cpu->afBank[cpu->activeAFBank].af.high;
}

static void cpuz80_load8_ra(struct cpuz80* cpu) {
    /* LD R,A */
    cpu->ir.low = cpu->afBank[cpu->activeAFBank].af.high;
}

static void cpuz80_load16_ixn(struct cpuz80* cpu) {
    /* LD IX,nn */
    cpu->ix = cpuz80_get_imm16(cpu);
}

static void cpuz80_load16_iyn(struct cpuz80* cpu) {
    /* LD IY,nn */
    cpu->iy = cpuz80_get_imm16(cpu);
}

static void cpuz80_load16_indirectrpn(struct cpuz80* cpu) {
    /* LD dd,(nn) */
    const uint16_t value = cpuz80_getValue_ind16(cpu);
    cpuz80_setValue_registerPair(cpu, cpu->decoder.ir.xpqz.p, value);
}

static void cpuz80_load16_indirectixn(struct cpuz80* cpu) {
    /* LD IX,(nn) */
    cpu->ix = cpuz80_getValue_ind16(cpu);
}

static void cpuz80_load16_indirectiyn(struct cpuz80* cpu) {
    /* LD IY,(nn) */
    cpu->iy = cpuz80_getValue_ind16(cpu);
}

static void cpuz80_load16_nix(struct cpuz80* cpu) {
    /* LD (nn),IX */
    cpuz80_setValue_ind16(cpu, cpu->ix);
}

static void cpuz80_load16_niy(struct cpuz80* cpu) {
    /* LD (nn),IY */
    cpuz80_setValue_ind16(cpu, cpu->iy);
}

static void cpuz80_load16_spix(struct cpuz80* cpu) {
    /* LD SP,IX */
    cpu->sp = cpu->ix;
}

static void cpuz80_load16_spiy(struct cpuz80* cpu) {
    /* LD SP,IY */
    cpu->sp = cpu->iy;
}

static void cpuz80_push_ix(struct cpuz80* cpu) {
    /* PUSH IX */
    cpuz80_push16(cpu, cpu->ix);
}

static void cpuz80_push_iy(struct cpuz80* cpu) {
    /* PUSH IY */
    cpuz80_push16(cpu, cpu->iy);
}

static void cpuz80_pop_rix(struct cpuz80* cpu) {
    /* POP IX */
    cpu->ix = cpuz80_pop16(cpu);
}

static void cpuz80_pop_riy(struct cpuz80* cpu) {
    /* POP IY */
    cpu->iy = cpuz80_pop16(cpu);
}

static void cpuz80_ex_spix(struct cpuz80* cpu) {
    /* EX (SP),IX */
    const uint8_t tmpl = cpu->l(cpu->sp, false, cpu->userData);
    const uint8_t tmph = cpu->l(cpu->sp + 1, false, cpu->userData);
    cpu->s(cpu->sp, false, cpu->ix & 0xff, cpu->userData);
    cpu->s(cpu->sp + 1, false, cpu->ix >> 8, cpu->userData);
    cpu->ix = cpuz80_makeWord(tmph, tmpl);
}

static void cpuz80_ex_spiy(struct cpuz80* cpu) {
    /* EX (SP),IY */
    const uint8_t tmpl = cpu->l(cpu->sp, false, cpu->userData);
    const uint8_t tmph = cpu->l(cpu->sp + 1, false, cpu->userData);
    cpu->s(cpu->sp, false, cpu->iy & 0xff, cpu->userData);
    cpu->s(cpu->sp + 1, false, cpu->iy >> 8, cpu->userData);
    cpu->iy = cpuz80_makeWord(tmph, tmpl);
}

static void cpuz80_ldi(struct cpuz80* cpu) {
    /* LDI */
    const uint16_t srca = cpuz80_registerPairPostIncrement(
            &cpu->bank[cpu->activeBank].hl);
    const uint16_t dsta = cpuz80_registerPairPostIncrement(
            &cpu->bank[cpu->activeBank].de);

    const uint8_t data = cpu->l(srca, false, cpu->userData);
    cpu->s(dsta, false, data, cpu->userData);

    const uint16_t bc =
        cpuz80_registerPairPreDecrement(&cpu->bank[cpu->activeBank].bc);

    uint8_t n = (cpu->afBank[cpu->activeAFBank].af.high + data);
    n = ((n & 0x02) << 4) | (n & (0x01 << 3));

    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, n);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, (bc != 0));
}

static void cpuz80_ldir(struct cpuz80* cpu) {
    /* LDIR */
    uint8_t n = 0;

    do {
        const uint16_t srca = cpuz80_registerPairPostIncrement(
                &cpu->bank[cpu->activeBank].hl);
        const uint16_t dsta = cpuz80_registerPairPostIncrement(
                &cpu->bank[cpu->activeBank].de);

        n = cpu->l(srca, false, cpu->userData);
        cpu->s(dsta, false, n, cpu->userData);

        const uint16_t bc =
            cpuz80_registerPairPreDecrement(&cpu->bank[cpu->activeBank].bc);

        CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, (bc != 0));
    } while (CPUZ80_TEST_PV(cpu->afBank[cpu->activeAFBank].af.low));

    if (cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].bc)) {
        cpuz80_tick(cpu, 1, 5);
    }

    n += cpu->afBank[cpu->activeAFBank].af.high;
    n = ((n & 0x02) << 4) | (n & (0x01 << 3));
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, n);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
}

static void cpuz80_ldd(struct cpuz80* cpu) {
    /* LDD */
    const uint16_t srca = cpuz80_registerPairPostDecrement(
            &cpu->bank[cpu->activeBank].hl);
    const uint16_t dsta = cpuz80_registerPairPostDecrement(
            &cpu->bank[cpu->activeBank].de);
    const uint8_t data = cpu->l(srca, false, cpu->userData);
    cpu->s(dsta, false, data, cpu->userData);
    const uint16_t bc =
        cpuz80_registerPairPreDecrement(&cpu->bank[cpu->activeBank].bc);

    uint8_t n = (cpu->afBank[cpu->activeAFBank].af.high + data);
    n = ((n & 0x02) << 4) | (n & (0x01 << 3));

    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, n);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, bc);
}

static void cpuz80_lddr(struct cpuz80* cpu) {
    /* LDDR */
    uint8_t n = 0;

    if (cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].bc)) {
        cpuz80_tick(cpu, 1, 5);
    }

    do {
        const uint16_t srca = cpuz80_registerPairPostDecrement(
                &cpu->bank[cpu->activeBank].hl);
        const uint16_t dsta = cpuz80_registerPairPostDecrement(
                &cpu->bank[cpu->activeBank].de);
        n = cpu->l(srca, false, cpu->userData);
        cpu->s(dsta, false, n, cpu->userData);

        const uint16_t bc =
            cpuz80_registerPairPreDecrement(&cpu->bank[cpu->activeBank].bc);

        CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
        CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
        CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, bc);
    } while (CPUZ80_TEST_PV(cpu->afBank[cpu->activeAFBank].af.low));

    n += cpu->afBank[cpu->activeAFBank].af.high;
    n = ((n & 0x02) << 4) | (n & (0x01 << 3));
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, n);
}

static void cpuz80_cpi(struct cpuz80* cpu) {
    /* CPI */
    const uint8_t value = cpu->l(cpuz80_registerPairPostIncrement(
                &cpu->bank[cpu->activeBank].hl), false, cpu->userData);
    const uint8_t result = cpu->afBank[cpu->activeAFBank].af.high - value;
    const uint16_t bc = cpuz80_registerPairPreDecrement(
            &cpu->bank[cpu->activeBank].bc);

    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, bc);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high,
            value,
            result);
    uint8_t n = result - CPUZ80_GET_H(cpu->afBank[cpu->activeAFBank].af.low);
    n = ((n & 0x02) << 4) | (n & (0x01 << 3));
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, n);
}

static void cpuz80_cpir(struct cpuz80* cpu) {
    /* CPIR */
    uint8_t result = 0;
    if (cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].bc)) {
        cpuz80_tick(cpu, 1, 5);
    }
    do {
        const uint8_t value = cpu->l(cpuz80_registerPairPostIncrement(
                    &cpu->bank[cpu->activeBank].hl), false, cpu->userData);
        result = cpu->afBank[cpu->activeAFBank].af.high - value;
        const uint16_t bc = cpuz80_registerPairPreDecrement(
                &cpu->bank[cpu->activeBank].bc);

        CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, bc);
        CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
        CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
        CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low,
                cpu->afBank[cpu->activeAFBank].af.high,
                value,
                result);
    } while (CPUZ80_TEST_PV(cpu->afBank[cpu->activeAFBank].af.low) &&
            !CPUZ80_TEST_Z(cpu->afBank[cpu->activeAFBank].af.low));

    uint8_t n = result - CPUZ80_GET_H(cpu->afBank[cpu->activeAFBank].af.low);
    n = ((n & 0x02) << 4) | (n & (0x01 << 3));
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, n);
    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
}

static void cpuz80_cpd(struct cpuz80* cpu) {
    /* CPD */
    const uint8_t value = cpu->l(
            cpuz80_registerPairPostDecrement(&cpu->bank[cpu->activeBank].hl),
            false,
            cpu->userData);
    const uint8_t result = cpu->afBank[cpu->activeAFBank].af.high - value;

    const uint16_t bc =
        cpuz80_registerPairPreDecrement(&cpu->bank[cpu->activeBank].bc);

    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, bc);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (result == 0x00));
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (result & 0x80));
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high,
            value,
            result);

    uint8_t n = result - CPUZ80_GET_H(cpu->afBank[cpu->activeAFBank].af.low);
    n = ((n & 0x02) << 4) | (n & (0x01 << 3));
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, n);
}

static void cpuz80_cpdr(struct cpuz80* cpu) {
    /* CPDR */
    uint8_t result = 0;
    if (cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].bc)) {
        cpuz80_tick(cpu, 1, 5);
    }
    do {
        const uint8_t value = cpu->l(
                cpuz80_registerPairPostDecrement(&cpu->bank[cpu->activeBank].hl),
                false,
                cpu->userData);
        result = cpu->afBank[cpu->activeAFBank].af.high - value;
        uint16_t bc = cpuz80_registerPairPreDecrement(&cpu->bank[cpu->activeBank].bc);

        CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, bc);
        CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (result == 0x00));
        CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (result & 0x80));
        CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low,
                cpu->afBank[cpu->activeAFBank].af.high,
                value,
                result);

    } while (CPUZ80_TEST_PV(cpu->afBank[cpu->activeAFBank].af.low) &&
            !CPUZ80_TEST_Z(cpu->afBank[cpu->activeAFBank].af.low));

    uint8_t n = result - CPUZ80_GET_H(cpu->afBank[cpu->activeAFBank].af.low);
    n = ((n & 0x02) << 4) | (n & (0x01 << 3));
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, n);
    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
}

static void cpuz80_alu_add8(struct cpuz80* cpu, uint8_t a) {
    /* ADD A,s */
    const uint16_t r16 = cpu->afBank[cpu->activeAFBank].af.high + a;
    const uint8_t r8 = r16 & 0xff;

    CPUZ80_CALC_C(cpu->afBank[cpu->activeAFBank].af.low, r16);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_PV_ADD(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high, a, r8);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high, a, r8);

    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (r8 == 0x00));
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (r8 & 0x80));
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, r8);

    // save the result in accumulator
    cpu->afBank[cpu->activeAFBank].af.high = r8;
}

static void cpuz80_alu_adc8(struct cpuz80* cpu, uint8_t a) {
    /* ADC A,s */
    const uint8_t c = CPUZ80_GET_C(cpu->afBank[cpu->activeAFBank].af.low);
    const uint16_t r16 = cpu->afBank[cpu->activeAFBank].af.high + a + c;
    const uint8_t r8 = r16 & 0xff;

    CPUZ80_CALC_C(cpu->afBank[cpu->activeAFBank].af.low, r16);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_PV_ADD(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high, a, r8);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high, a, r8);

    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (r8 == 0x00));
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (r8 & 0x80));
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, r8);

    // save the result in accumulator
    cpu->afBank[cpu->activeAFBank].af.high = r8;
}

static void cpuz80_alu_sub8(struct cpuz80* cpu, uint8_t a) {
    /* SUB A,s */
    const uint16_t r16 = cpu->afBank[cpu->activeAFBank].af.high - a;
    const uint8_t r8 = r16 & 0xff;

    CPUZ80_CALC_C(cpu->afBank[cpu->activeAFBank].af.low, r16);
    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_PV_SUB(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high, a, r8);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high, a, r8);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (r8 == 0x00));
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (r8 & 0x80));
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, r8);

    // save the result in accumulator
    cpu->afBank[cpu->activeAFBank].af.high = r8;
}

static void cpuz80_alu_sbc8(struct cpuz80* cpu, uint8_t a) {
    /* SBC A,s */
    const uint8_t c = CPUZ80_GET_C(cpu->afBank[cpu->activeAFBank].af.low);
    const uint16_t r16 = cpu->afBank[cpu->activeAFBank].af.high - a - c;
    const uint8_t r8 = r16 & 0xff;

    CPUZ80_CALC_C(cpu->afBank[cpu->activeAFBank].af.low, r16);
    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_PV_SUB(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high, a, r8);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high, a, r8);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (r8 == 0x00));
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (r8 & 0x80));
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, r8);

    // save the result in accumulator
    cpu->afBank[cpu->activeAFBank].af.high = r8;
}

static void cpuz80_alu_and8(struct cpuz80* cpu, uint8_t a) {
    /* AND A,s */
    uint8_t r8 = cpu->afBank[cpu->activeAFBank].af.high & a;
    cpu->afBank[cpu->activeAFBank].af.high = r8;
    CPUZ80_CLEAR_C(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_SET_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, r8);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (r8 == 0x00));
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (r8 & 0x80));
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, r8);
}

static void cpuz80_alu_or8(struct cpuz80* cpu, uint8_t a) {
    /* OR A,s */
    uint8_t result = cpu->afBank[cpu->activeAFBank].af.high | (a & 0xff);
    cpu->afBank[cpu->activeAFBank].af.high = result;
    CPUZ80_CLEAR_C(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (result == 0x00));
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (result & 0x80));
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_alu_xor8(struct cpuz80* cpu, uint8_t a) {
    /* XOR A,s */
    uint8_t result = cpu->afBank[cpu->activeAFBank].af.high ^ a;
    cpu->afBank[cpu->activeAFBank].af.high = result;
    CPUZ80_CLEAR_C(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (result == 0x00));
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (result & 0x80));
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_alu_cp8(struct cpuz80* cpu, uint8_t a) {
    /* CP A,s */
    const uint16_t r16 = cpu->afBank[cpu->activeAFBank].af.high - a;
    const uint8_t r8 = r16 & 0xff;

    CPUZ80_CALC_C(cpu->afBank[cpu->activeAFBank].af.low, r16);
    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_PV_SUB(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high, a, r8);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high, a, r8);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (r8 == 0x00));
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (r8 & 0x80));
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, a);
}

static uint8_t cpuz80_alu8_getValue(struct cpuz80* cpu) {
    uint8_t value = 0;
    // decode value
    switch(cpu->decoder.ir.xyz.x) {
        case 0x02:
            /* OP A,r */
            /* OP A,(HL) */
            /* OP A,(IX+d) */
            /* OP A,(IY+d) */
            switch(cpu->decoder.prefix) {
                case 0x00:
                    value = cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.z);
                    break;

                case 0xdd:
                    switch (cpu->decoder.ir.xyz.z) {
                        case 0x06:
                            cpu->decoder.d = cpuz80_get_imm8(cpu);
                            value = cpuz80_getValue_ixd(cpu);
                            break;

                        default:
                            value = cpuz80_getValue_register2(cpu,
                                    cpu->decoder.ir.xyz.z);
                            break;
                    }
                    break;
                case 0xfd:
                    switch (cpu->decoder.ir.xyz.z) {
                        case 0x06:
                            cpu->decoder.d = cpuz80_get_imm8(cpu);
                            value = cpuz80_getValue_iyd(cpu);
                            break;

                        default:
                            value = cpuz80_getValue_register3(cpu,
                                    cpu->decoder.ir.xyz.z);
                            break;
                    }
                    break;
            }
            break;

        case 0x03:
            /* OP A,(n) */
            switch(cpu->decoder.prefix) {
                case 0x00:
                    value = cpuz80_get_imm8(cpu);
                    break;
            }
            break;
    }

    return value;
}

static void cpuz80_alu8(struct cpuz80* cpu) {
    const uint8_t value = cpuz80_alu8_getValue(cpu);

    // decode operation
    switch(cpu->decoder.ir.xyz.y) {
        /* ADD */
        case 0x00: cpuz80_alu_add8(cpu, value); break;

        /* ADC */
        case 0x01: cpuz80_alu_adc8(cpu, value); break;

        /* SUB */
        case 0x02: cpuz80_alu_sub8(cpu, value); break;

        /* SBC */
        case 0x03: cpuz80_alu_sbc8(cpu, value); break;

        /* AND */
        case 0x04: cpuz80_alu_and8(cpu, value); break;

        /* XOR */
        case 0x05: cpuz80_alu_xor8(cpu, value); break;

        /* OR */
        case 0x06: cpuz80_alu_or8(cpu, value); break;

        /* CP */
        case 0x07: cpuz80_alu_cp8(cpu, value); break;
    }
}

static void cpuz80_inc_r(struct cpuz80* cpu) {
    /* INC r */
    /* INC (HL) */
    const uint8_t value = cpuz80_registerPostIncrement(cpu, cpu->decoder.ir.xyz.y);
    const uint8_t result = cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.y);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, value == 0x7f);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low, value, 1, result);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_inc_p(struct cpuz80* cpu) {
    /* INC p */
    const uint8_t value = cpuz80_registerPostIncrement2(cpu, cpu->decoder.ir.xyz.y);
    const uint8_t result = cpuz80_getValue_register2(cpu, cpu->decoder.ir.xyz.y);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, value == 0x7f);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low, value, 1, result);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_inc_q(struct cpuz80* cpu) {
    /* INC q */
    const uint8_t value = cpuz80_registerPostIncrement3(cpu, cpu->decoder.ir.xyz.y);
    const uint8_t result = cpuz80_getValue_register3(cpu, cpu->decoder.ir.xyz.y);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, value == 0x7f);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low, value, 1, result);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_inc_ixd(struct cpuz80* cpu) {
    /* INC (IX+d) */
    cpu->decoder.d = cpuz80_get_imm8(cpu);
    const uint8_t value = cpuz80_getValue_ixd(cpu);
    const uint8_t result = value + 1;
    cpuz80_setValue_ixd(cpu, result);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, value == 0x7f);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low, value, 1, result);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_inc_iyd(struct cpuz80* cpu) {
    /* INC (IY+d) */
    cpu->decoder.d = cpuz80_get_imm8(cpu);
    const uint8_t value = cpuz80_getValue_iyd(cpu);
    const uint8_t result = value + 1;
    cpuz80_setValue_iyd(cpu, result);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, value == 0x7f);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low, value, 1, result);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_dec_r(struct cpuz80* cpu) {
    /* DEC r */
    /* DEC (HL) */
    const uint8_t value = cpuz80_registerPostDecrement(cpu, cpu->decoder.ir.xyz.y);
    const uint8_t result = cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.y);
    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, value == 0x80);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low, value, 1, result);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_dec_p(struct cpuz80* cpu) {
    /* DEC p */
    const uint8_t value = cpuz80_registerPostDecrement2(cpu, cpu->decoder.ir.xyz.y);
    const uint8_t result = cpuz80_getValue_register2(cpu, cpu->decoder.ir.xyz.y);
    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, value == 0x80);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low, value, 1, result);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_dec_q(struct cpuz80* cpu) {
    /* DEC q */
    const uint8_t value = cpuz80_registerPostDecrement3(cpu, cpu->decoder.ir.xyz.y);
    const uint8_t result = cpuz80_getValue_register3(cpu, cpu->decoder.ir.xyz.y);
    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, value == 0x80);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low, value, 1, result);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_dec_ss(struct cpuz80* cpu) {
    /* DEC ss */
    const uint16_t a = cpuz80_getValue_registerPair(cpu, cpu->decoder.ir.xpqz.p);
    const uint16_t result = a - 1;
    cpuz80_setValue_registerPair(cpu, cpu->decoder.ir.xpqz.p, result);
}

static void cpuz80_inc_ss(struct cpuz80* cpu) {
    /* INC ss */
    const uint16_t a = cpuz80_getValue_registerPair(cpu, cpu->decoder.ir.xpqz.p);
    const uint16_t result = a + 1;
    cpuz80_setValue_registerPair(cpu, cpu->decoder.ir.xpqz.p, result);
}

static void cpuz80_dec_ixd(struct cpuz80* cpu) {
    /* DEC (IX+d) */
    cpu->decoder.d = cpuz80_get_imm8(cpu);
    const uint8_t value = cpuz80_getValue_ixd(cpu);
    const uint8_t result = value - 1;
    cpuz80_setValue_ixd(cpu, result);
    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, value == 0x80);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low, value, 1, result);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_dec_iyd(struct cpuz80* cpu) {
    /* DEC (IY+d) */
    cpu->decoder.d = cpuz80_get_imm8(cpu);
    const uint8_t value = cpuz80_getValue_iyd(cpu);
    const uint8_t result = value - 1;
    cpuz80_setValue_iyd(cpu, result);
    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, value == 0x80);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low, value, 1, result);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_daa(struct cpuz80* cpu) {
    /* DAA */
    uint16_t value = cpu->afBank[cpu->activeAFBank].af.high;
    const uint8_t flags = cpu->afBank[cpu->activeAFBank].af.low;
    uint8_t diff = 0;
    uint8_t low = value & 0x0f;
    uint8_t high = value & 0xf0;

    if (CPUZ80_TEST_N(flags)) {
        // last operation was a SUB
        int hd = CPUZ80_TEST_C(flags) || (value > 0x99);
        if ((low > 0x09) || CPUZ80_TEST_H(flags)) {
            if (low > 0x05) {
                CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
            }
            value -= 0x06;
            value &= 0xff;
        }
        if (hd) {
            value -= 0x160;
        }
    }
    else {
        // last operation was an ADD
        if ((low > 0x09) || CPUZ80_TEST_H(flags)) {
            CPUZ80_ASSIGN_H(cpu->afBank[cpu->activeAFBank].af.low, (low > 0x09));
            value += 0x06;
        }

        if (CPUZ80_TEST_C(flags) || ((value & 0x1f0) > 0x90)) {
            value += 0x60;
        }
    }

    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, ((value & 0xff00) || flags & 0x01));

    value &= 0xff;
    cpu->afBank[cpu->activeAFBank].af.high = value;
    uint8_t a = cpu->afBank[cpu->activeAFBank].af.high;
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, a);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (value == 0x00));
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, value);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (value & 0x80));
}

static void cpuz80_cpl(struct cpuz80* cpu) {
    /* CPL */
    cpu->afBank[cpu->activeAFBank].af.high =
        ~cpu->afBank[cpu->activeAFBank].af.high;
    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_SET_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high);
}

static void cpuz80_neg(struct cpuz80* cpu) {
    /* NEG */
    uint8_t result =
        0 - cpu->afBank[cpu->activeAFBank].af.high;

    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high != 0x00);
    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high == 0x80);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high, 0, result);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);

    // save the result in accumulator
    cpu->afBank[cpu->activeAFBank].af.high = result;
}

static void cpuz80_ccf(struct cpuz80* cpu) {
    /* CCF */
    CPUZ80_ASSIGN_H(cpu->afBank[cpu->activeAFBank].af.low,
            CPUZ80_GET_C(cpu->afBank[cpu->activeAFBank].af.low));
    CPUZ80_TOGGLE_C(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, cpu->afBank[cpu->activeAFBank].af.high);
}

static void cpuz80_scf(struct cpuz80* cpu) {
    /* SCF */
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_SET_C(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, cpu->afBank[cpu->activeAFBank].af.high);
}

static void cpuz80_halt(struct cpuz80* cpu) {
    /* HALT */
    cpu->isHalted = true;
}

static void cpuz80_di(struct cpuz80* cpu) {
    /* DI */
    cpu->iff1 = 0x00;
    cpu->iff2 = 0x00;
}

static void cpuz80_ei(struct cpuz80* cpu) {
    /* EI */
    cpu->iff1 = 0x01;
    cpu->iff2 = 0x01;
}

static void cpuz80_im0(struct cpuz80* cpu) {
    /* IM0 */
    cpu->interruptMode = 0x00;
}

static void cpuz80_im1(struct cpuz80* cpu) {
    /* IM1 */
    cpu->interruptMode = 0x01;
}

static void cpuz80_im2(struct cpuz80* cpu) {
    /* IM2 */
    cpu->interruptMode = 0x02;
}

static void cpuz80_add16_yxiyhlss(struct cpuz80* cpu) {
    /* ADD HL,ss */
    /* ADD IX,ss */
    /* ADD IY,ss */
    const uint16_t a = cpuz80_getValue_registerPair3(cpu, 2);
    const uint16_t b = cpuz80_getValue_registerPair3(cpu, cpu->decoder.ir.xpqz.p);

    const uint8_t al8 = a & 0xff;
    const uint8_t bl8 = b & 0xff;
    const uint16_t rl16 = al8 + bl8;
    const uint8_t lCarry = (rl16 & 0xff00) != 0x00;
    const uint8_t ah8 = a >> 8;
    const uint8_t bh8 = b >> 8;

    const uint16_t rh16 = ah8 + bh8 + lCarry;
    const uint32_t r32 = (((uint32_t)rh16) << 8) | (rl16 & 0xff);
    const uint16_t r16 = r32 & 0xffff;
    const uint8_t rh8 = r16 >> 8;

    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, (r32 & 0xffff0000));
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, rh16);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low, ah8, bh8, rh8);
    cpuz80_setValue_registerPair3(cpu, 2, r16);
}

static void cpuz80_adc16_hlss(struct cpuz80* cpu) {
    /* ADC HL,ss */
    const uint16_t a = cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].hl);
    const uint16_t b = cpuz80_getValue_registerPair(cpu, cpu->decoder.ir.xpqz.p);

    const uint8_t al8 = a & 0xff;
    const uint8_t bl8 = b & 0xff;
    const uint16_t rl16 = al8 + bl8;
    const uint8_t lCarry = (rl16 & 0xff00) != 0x00;
    const uint8_t ah8 = a >> 8;
    const uint8_t bh8 = b >> 8;

    const uint16_t rh16 = ah8 + bh8 + lCarry;
    const uint8_t c = CPUZ80_GET_C(cpu->afBank[cpu->activeAFBank].af.low);
    const uint32_t r32 = ((((uint32_t)rh16) << 8) | (rl16 & 0xff)) + c;
    const uint16_t r16 = r32 & 0xffff;
    const uint8_t rh8 = r16 >> 8;

    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, (r32 & 0xffff0000));
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, rh8);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low, ah8, bh8, rh8);
    CPUZ80_CALC_PV_ADD(cpu->afBank[cpu->activeAFBank].af.low, ah8, bh8, rh16);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (r16 & 0x8000));
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (r16 == 0x00));

    cpuz80_wordToRegisterPair(&cpu->bank[cpu->activeBank].hl, r16);
}

static void cpuz80_sbc16_hlss(struct cpuz80* cpu) {
    /* SBC HL,ss */
    const uint16_t a = cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].hl);
    const uint16_t b = cpuz80_getValue_registerPair(cpu, cpu->decoder.ir.xpqz.p);

    const uint8_t al8 = a & 0xff;
    const uint8_t bl8 = b & 0xff;
    const uint16_t rl16 = al8 - bl8;
    const uint8_t lCarry = (rl16 & 0xff00) != 0x00;
    const uint8_t ah8 = a >> 8;
    const uint8_t bh8 = b >> 8;

    const uint16_t rh16 = ah8 - bh8 - lCarry;
    const uint8_t c = CPUZ80_GET_C(cpu->afBank[cpu->activeAFBank].af.low);
    const uint32_t r32 = ((((uint32_t)rh16) << 8) | (rl16 & 0xff)) - c;
    const uint16_t r16 = r32 & 0xffff;
    const uint8_t rh8 = r16 >> 8;

    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, (r32 & 0xffff0000));
    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, rh8);
    CPUZ80_CALC_H(cpu->afBank[cpu->activeAFBank].af.low, ah8, bh8, rh8);
    CPUZ80_CALC_PV_SUB(cpu->afBank[cpu->activeAFBank].af.low, ah8, bh8, (r32 >> 8));
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (r16 & 0x8000));
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (r16 == 0x00));

    cpuz80_wordToRegisterPair(&cpu->bank[cpu->activeBank].hl, r16);
}

static void cpuz80_inc16_ixiyss(struct cpuz80* cpu) {
    /* INC ss */
    /* INC IX */
    /* INC IY */
    const uint16_t value = cpuz80_getValue_registerPair3(cpu, cpu->decoder.ir.xpqz.p);
    const uint16_t result = value + 1;
    cpuz80_setValue_registerPair3(cpu, cpu->decoder.ir.xpqz.p, result);
}

static void cpuz80_dec16_ixiyss(struct cpuz80* cpu) {
    /* DEC ss */
    /* DEC IX */
    /* DEC IY */
    const uint16_t value = cpuz80_getValue_registerPair3(cpu, cpu->decoder.ir.xpqz.p);
    const uint16_t result = value - 1;
    cpuz80_setValue_registerPair3(cpu, cpu->decoder.ir.xpqz.p, result);
}

static void cpuz80_rlca(struct cpuz80* cpu) {
    /* RLCA */
    const uint8_t signBit =
        CPUZ80_BIT_GET(cpu->afBank[cpu->activeAFBank].af.high, 7);

    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, signBit);
    cpu->afBank[cpu->activeAFBank].af.high <<= 1;
    cpu->afBank[cpu->activeAFBank].af.high |= signBit;
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high);
}

static void cpuz80_rla(struct cpuz80* cpu) {
    /* RLA */
    const uint8_t signBit =
        CPUZ80_BIT_GET(cpu->afBank[cpu->activeAFBank].af.high, 7);

    const uint8_t oldCarry =
        CPUZ80_GET_C(cpu->afBank[cpu->activeAFBank].af.low);

    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, signBit);
    cpu->afBank[cpu->activeAFBank].af.high <<= 1;
    cpu->afBank[cpu->activeAFBank].af.high |= oldCarry;
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, cpu->afBank[cpu->activeAFBank].af.high);
}

static void cpuz80_rrca(struct cpuz80* cpu) {
    /* RRCA */
    const uint8_t zeroBit =
        CPUZ80_BIT_GET(cpu->afBank[cpu->activeAFBank].af.high, 0);

    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, zeroBit);
    cpu->afBank[cpu->activeAFBank].af.high >>= 1;
    cpu->afBank[cpu->activeAFBank].af.high |= (zeroBit << 7);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low,
            cpu->afBank[cpu->activeAFBank].af.high);
}

static void cpuz80_rra(struct cpuz80* cpu) {
    /* RRA */
    const uint8_t zeroBit =
        CPUZ80_BIT_GET(cpu->afBank[cpu->activeAFBank].af.high, 0);

    const uint8_t oldCarry =
        CPUZ80_GET_C(cpu->afBank[cpu->activeAFBank].af.low);

    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, zeroBit);
    cpu->afBank[cpu->activeAFBank].af.high >>= 1;
    cpu->afBank[cpu->activeAFBank].af.high |= (oldCarry << 7);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, cpu->afBank[cpu->activeAFBank].af.high);
}

static void cpuz80_rlc_r(struct cpuz80* cpu) {
    /* RLC r */
    /* RLC (HL) */
    uint8_t result = cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.z);
    const uint8_t msb = CPUZ80_BIT_GET(result, 7);
    result <<= 1;
    if (msb) {
        CPUZ80_BIT_SET(result, 0);
    }
    if (0x06 == cpu->decoder.ir.xyz.z) {
        cpuz80_tick(cpu, 2, 7);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (result & 0x80));
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (result == 0x00));
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, msb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_register(cpu, cpu->decoder.ir.xyz.z, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_rlc_ixd(struct cpuz80* cpu) {
    /* RLC (IX+d) */
    uint8_t result = cpuz80_getValue_ixd(cpu);
    const uint8_t msb = CPUZ80_BIT_GET(result, 7);
    result <<= 1;
    if (msb) {
        CPUZ80_BIT_SET(result, 0);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, msb);
    cpuz80_setValue_ixd(cpu, result);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_rlc_ixdr(struct cpuz80* cpu) {
    /* RLC (IX+d), r */
    uint8_t value = cpuz80_getValue_ixd(cpu);
    cpuz80_setValue_register(cpu, cpu->decoder.ir.xyz.z, value);
    cpuz80_rlc_r(cpu);
    value = cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.z);
    cpuz80_setValue_ixd(cpu, value);
}

static void cpuz80_rlc_iyd(struct cpuz80* cpu) {
    /* RLC (IY+d) */
    uint8_t result = cpuz80_getValue_iyd(cpu);
    const uint8_t msb = CPUZ80_BIT_GET(result, 7);
    result <<= 1;
    if (msb) {
        CPUZ80_BIT_SET(result, 0);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, msb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_iyd(cpu, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_rl_r(struct cpuz80* cpu) {
    /* RL r */
    /* RL (HL) */
    uint8_t result = cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.z);
    const uint8_t msb = CPUZ80_BIT_GET(result, 7);
    result <<= 1;
    if (CPUZ80_TEST_C(cpu->afBank[cpu->activeAFBank].af.low)) {
        CPUZ80_BIT_SET(result, 0);
    }
    if (0x06 == cpu->decoder.ir.xyz.z) {
        cpuz80_tick(cpu, 2, 7);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (result & 0x80));
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (result == 0x00));
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, msb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_register(cpu, cpu->decoder.ir.xyz.z, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_rl_ixd(struct cpuz80* cpu) {
    /* RL (IX+d) */
    uint8_t result = cpuz80_getValue_ixd(cpu);
    const uint8_t msb = CPUZ80_BIT_GET(result, 7);
    result <<= 1;
    if (CPUZ80_TEST_C(cpu->afBank[cpu->activeAFBank].af.low)) {
        CPUZ80_BIT_SET(result, 0);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, msb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_ixd(cpu, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_rl_iyd(struct cpuz80* cpu) {
    /* RL (IY+d) */
    uint8_t result = cpuz80_getValue_iyd(cpu);
    const uint8_t msb = CPUZ80_BIT_GET(result, 7);
    result <<= 1;
    if (CPUZ80_TEST_C(cpu->afBank[cpu->activeAFBank].af.low)) {
        CPUZ80_BIT_SET(result, 0);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, msb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_iyd(cpu, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_rrc_r(struct cpuz80* cpu) {
    /* RRC r */
    /* RRC (HL) */
    uint8_t result = cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.z);
    const uint8_t lsb = CPUZ80_BIT_GET(result, 0);
    result >>= 1;
    if (lsb) {
        CPUZ80_BIT_SET(result, 7);
    }
    if (0x06 == cpu->decoder.ir.xyz.z) {
        cpuz80_tick(cpu, 2, 7);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (result & 0x80));
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (result == 0x00));
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, lsb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_register(cpu, cpu->decoder.ir.xyz.z, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_rrc_ixd(struct cpuz80* cpu) {
    /* RRC (IX+d) */
    uint8_t result = cpuz80_getValue_ixd(cpu);
    const uint8_t lsb = CPUZ80_BIT_GET(result, 0);
    result >>= 1;
    if (lsb) {
        CPUZ80_BIT_SET(result, 7);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, lsb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_ixd(cpu, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_rrc_iyd(struct cpuz80* cpu) {
    /* RRC (IY+d) */
    uint8_t result = cpuz80_getValue_iyd(cpu);
    const uint8_t lsb = CPUZ80_BIT_GET(result, 0);
    result >>= 1;
    if (lsb) {
        CPUZ80_BIT_SET(result, 7);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, lsb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_iyd(cpu, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_rr_r(struct cpuz80* cpu) {
    /* RR r */
    /* RR (HL) */
    uint8_t result = cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.z);
    const uint8_t lsb = CPUZ80_BIT_GET(result, 0);
    result >>= 1;
    if (CPUZ80_TEST_C(cpu->afBank[cpu->activeAFBank].af.low)) {
        CPUZ80_BIT_SET(result, 7);
    }
    if (0x06 == cpu->decoder.ir.xyz.z) {
        cpuz80_tick(cpu, 2, 7);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (result & 0x80));
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (result == 0x00));
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, lsb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_register(cpu, cpu->decoder.ir.xyz.z, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_rr_ixd(struct cpuz80* cpu) {
    /* RR (IX+d) */
    uint8_t result = cpuz80_getValue_ixd(cpu);
    const uint8_t lsb = CPUZ80_BIT_GET(result, 0);
    result >>= 1;
    if (CPUZ80_TEST_C(cpu->afBank[cpu->activeAFBank].af.low)) {
        CPUZ80_BIT_SET(result, 7);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, lsb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_ixd(cpu, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_rr_iyd(struct cpuz80* cpu) {
    /* RR (IY+d) */
    uint8_t result = cpuz80_getValue_iyd(cpu);
    const uint8_t lsb = CPUZ80_BIT_GET(result, 0);
    result >>= 1;
    if (CPUZ80_TEST_C(cpu->afBank[cpu->activeAFBank].af.low)) {
        CPUZ80_BIT_SET(result, 7);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, lsb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_iyd(cpu, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_sla_r(struct cpuz80* cpu) {
    /* SLA r */
    /* SLA (HL) */
    uint8_t result = cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.z);
    const uint8_t msb = CPUZ80_BIT_GET(result, 7);
    result <<= 1;
    if (0x06 == cpu->decoder.ir.xyz.z) {
        cpuz80_tick(cpu, 2, 7);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (result & 0x80));
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (result == 0x00));
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, msb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_register(cpu, cpu->decoder.ir.xyz.z, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_sll_r(struct cpuz80* cpu) {
    /* SLL r */
    /* SLL (HL) */
    uint8_t result = cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.z);
    const uint8_t msb = CPUZ80_BIT_GET(result, 7);
    result <<= 1;
    result |= 0x01;
    if (0x06 == cpu->decoder.ir.xyz.z) {
        cpuz80_tick(cpu, 2, 7);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (result & 0x80));
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (result == 0x00));
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, msb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_register(cpu, cpu->decoder.ir.xyz.z, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_sla_ixd(struct cpuz80* cpu) {
    /* SLA (IX+d) */
    uint8_t result = cpuz80_getValue_ixd(cpu);
    const uint8_t msb = CPUZ80_BIT_GET(result, 7);
    result <<= 1;
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, msb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_ixd(cpu, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_sll_ixd(struct cpuz80* cpu) {
    /* SLL (IX+d) */
    uint8_t result = cpuz80_getValue_ixd(cpu);
    const uint8_t msb = CPUZ80_BIT_GET(result, 7);
    result <<= 1;
    result |= 0x01;
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, msb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_ixd(cpu, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_sla_iyd(struct cpuz80* cpu) {
    /* SLA (IY+d) */
    uint8_t result = cpuz80_getValue_iyd(cpu);
    const uint8_t msb = CPUZ80_BIT_GET(result, 7);
    result <<= 1;
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, msb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_iyd(cpu, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_sll_iyd(struct cpuz80* cpu) {
    /* SLL (IY+d) */
    uint8_t result = cpuz80_getValue_iyd(cpu);
    const uint8_t msb = CPUZ80_BIT_GET(result, 7);
    result <<= 1;
    result |= 0x01;
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, msb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_iyd(cpu, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_sra_r(struct cpuz80* cpu) {
    /* SRA r */
    /* SRA (HL) */
    uint8_t result = cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.z);
    const uint8_t msb = CPUZ80_BIT_GET(result, 7);
    const uint8_t lsb = CPUZ80_BIT_GET(result, 0);
    result >>= 1;
    if (msb) {
        CPUZ80_BIT_SET(result, 7);
    }
    if (0x06 == cpu->decoder.ir.xyz.z) {
        cpuz80_tick(cpu, 2, 7);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (result & 0x80));
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (result == 0x00));
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, lsb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_register(cpu, cpu->decoder.ir.xyz.z, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_sra_ixd(struct cpuz80* cpu) {
    /* SRA (IX+d) */
    uint8_t result = cpuz80_getValue_ixd(cpu);
    const uint8_t msb = CPUZ80_BIT_GET(result, 7);
    const uint8_t lsb = CPUZ80_BIT_GET(result, 0);
    result >>= 1;
    if (msb) {
        CPUZ80_BIT_SET(result, 7);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, lsb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_ixd(cpu, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_sra_iyd(struct cpuz80* cpu) {
    /* SRA (IY+d) */
    uint8_t result = cpuz80_getValue_iyd(cpu);
    const uint8_t msb = CPUZ80_BIT_GET(result, 7);
    const uint8_t lsb = CPUZ80_BIT_GET(result, 0);
    result >>= 1;
    if (msb) {
        CPUZ80_BIT_SET(result, 7);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, lsb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_iyd(cpu, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_srl_r(struct cpuz80* cpu) {
    /* SRL r */
    /* SRL (HL) */
    uint8_t result = cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.z);
    const uint8_t lsb = CPUZ80_BIT_GET(result, 0);
    result >>= 1;
    if (0x06 == cpu->decoder.ir.xyz.z) {
        cpuz80_tick(cpu, 2, 7);
    }
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, (result & 0x80));
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (result == 0x00));
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, lsb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_register(cpu, cpu->decoder.ir.xyz.z, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_srl_ixd(struct cpuz80* cpu) {
    /* SRL (IX+d) */
    uint8_t result = cpuz80_getValue_ixd(cpu);
    const uint8_t lsb = CPUZ80_BIT_GET(result, 0);
    result >>= 1;
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, lsb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_ixd(cpu, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_srl_iyd(struct cpuz80* cpu) {
    /* SRL (IY+d) */
    uint8_t result = cpuz80_getValue_iyd(cpu);
    const uint8_t lsb = CPUZ80_BIT_GET(result, 0);
    result >>= 1;
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, result & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, result == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_C(cpu->afBank[cpu->activeAFBank].af.low, lsb);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, result);
    cpuz80_setValue_iyd(cpu, result);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, result);
}

static void cpuz80_rld(struct cpuz80* cpu) {
    /* RLD */
    uint8_t memByte = cpu->l(cpuz80_registerPairToWord(
                &cpu->bank[cpu->activeBank].hl), false, cpu->userData);
    const uint8_t tmp = memByte;

    memByte = (tmp << 4) | (cpu->afBank[cpu->activeAFBank].af.high & 0x0f);
    cpu->afBank[cpu->activeAFBank].af.high =
        (cpu->afBank[cpu->activeAFBank].af.high & 0xf0) |
        (tmp >> 4);

    uint8_t newA = cpu->afBank[cpu->activeAFBank].af.high;

    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, newA & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, newA == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, newA);
    cpu->s(cpuz80_registerPairToWord(
                &cpu->bank[cpu->activeBank].hl), false, memByte, cpu->userData);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, newA);
}

static void cpuz80_rrd(struct cpuz80* cpu) {
    /* RRD */
    uint8_t memByte = cpu->l(cpuz80_registerPairToWord(
                &cpu->bank[cpu->activeBank].hl), false, cpu->userData);
    const uint8_t tmp = memByte;

    memByte = (tmp >> 4) | (cpu->afBank[cpu->activeAFBank].af.high << 4);
    cpu->afBank[cpu->activeAFBank].af.high =
        (cpu->afBank[cpu->activeAFBank].af.high & 0xf0) | (tmp & 0x0f);
    uint8_t newA = cpu->afBank[cpu->activeAFBank].af.high;

    CPUZ80_CALC_X35(cpu->afBank[cpu->activeAFBank].af.low, newA);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, newA & 0x80);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, newA == 0x00);
    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, newA);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    cpu->s(cpuz80_registerPairToWord(
                &cpu->bank[cpu->activeBank].hl), false, memByte, cpu->userData);
}

static void cpuz80_bit_r(struct cpuz80* cpu) {
    /* BIT b,r */
    /* BIT b,(HL) */
    const uint8_t result = cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.z);
    const uint8_t n = cpu->decoder.ir.xyz.y;
    const uint8_t bit = CPUZ80_BIT_GET(result, n);

    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, ((n == 7) && (bit != 0x00)));
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_SET_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (bit == 0x00));
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, (bit == 0x00));

    // WARNING: discrepant with the documentation
    /* CPUZ80_ASSIGN_X5(cpu->afBank[cpu->activeAFBank].af.low, ((n == 5) && (bit != 0x00))); */
    /* CPUZ80_ASSIGN_X3(cpu->afBank[cpu->activeAFBank].af.low, ((n == 3) && (bit != 0x00))); */

    // I've shamelessly copied the X3,X5 flag logic from yaze implementation
    if (cpu->decoder.ir.xyz.z != 0x06) {
        cpu->afBank[cpu->activeAFBank].af.low |= (result & 0x28);
    }

    if (cpu->decoder.ir.xyz.z == 0x06) {
        cpuz80_tick(cpu, 1, 5);
    }
}

static void cpuz80_bit_ixd(struct cpuz80* cpu) {
    /* BIT b,(IX+d) */
    uint8_t result = cpuz80_getValue_ixd(cpu);
    const uint8_t n = cpu->decoder.ir.xyz.y;
    const uint8_t bit = CPUZ80_BIT_GET(result, n);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, ((n == 7) && (bit != 0x00)))
    // WARNING: discrepant with the documentation
    /* CPUZ80_ASSIGN_X5(cpu->afBank[cpu->activeAFBank].af.low, ((n == 5) && (bit != 0x00))) */
    /* CPUZ80_ASSIGN_X3(cpu->afBank[cpu->activeAFBank].af.low, ((n == 3) && (bit != 0x00))) */
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, (bit == 0x00));
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (bit == 0x00));
    CPUZ80_SET_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
}

static void cpuz80_bit_iyd(struct cpuz80* cpu) {
    /* BIT b,(IY+d) */
    uint8_t result = cpuz80_getValue_iyd(cpu);
    const uint8_t n = cpu->decoder.ir.xyz.y;
    const uint8_t bit = CPUZ80_BIT_GET(result, n);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, ((n == 7) && (bit != 0x00)))
    // WARNING: discrepant with the documentation
    /* CPUZ80_ASSIGN_X5(cpu->afBank[cpu->activeAFBank].af.low, ((n == 5) && (bit != 0x00))) */
    /* CPUZ80_ASSIGN_X3(cpu->afBank[cpu->activeAFBank].af.low, ((n == 3) && (bit != 0x00))) */
    CPUZ80_ASSIGN_PV(cpu->afBank[cpu->activeAFBank].af.low, (bit == 0x00));
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, (bit == 0x00));
    CPUZ80_SET_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
}

static void cpuz80_set_r(struct cpuz80* cpu) {
    /* SET b,r */
    /* SET b,(HL) */
    uint8_t result = cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.z);
    CPUZ80_BIT_SET(result, cpu->decoder.ir.xyz.y);
    cpuz80_setValue_register(cpu, cpu->decoder.ir.xyz.z, result);
    if (cpu->decoder.ir.xyz.z == 0x06) {
        cpuz80_tick(cpu, 2, 7);
    }
}

static void cpuz80_set_ixd(struct cpuz80* cpu) {
    /* SET b,(IX+d) */
    uint8_t result = cpuz80_getValue_ixd(cpu);
    CPUZ80_BIT_SET(result, cpu->decoder.ir.xyz.y);
    cpuz80_setValue_ixd(cpu, result);
}

static void cpuz80_set_iyd(struct cpuz80* cpu) {
    /* SET b,(IY+d) */
    uint8_t result = cpuz80_getValue_iyd(cpu);
    CPUZ80_BIT_SET(result, cpu->decoder.ir.xyz.y);
    cpuz80_setValue_iyd(cpu, result);
}

static void cpuz80_res_r(struct cpuz80* cpu) {
    /* RES b,r */
    /* RES b,(HL) */
    uint8_t result = cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.z);
    CPUZ80_BIT_CLEAR(result, cpu->decoder.ir.xyz.y);
    cpuz80_setValue_register(cpu, cpu->decoder.ir.xyz.z, result);
    if (cpu->decoder.ir.xyz.z == 0x06) {
        cpuz80_tick(cpu, 2, 7);
    }
}

static void cpuz80_res_ixd(struct cpuz80* cpu) {
    /* RES b,(IX+d) */
    uint8_t result = cpuz80_getValue_ixd(cpu);
    CPUZ80_BIT_CLEAR(result, cpu->decoder.ir.xyz.y);
    cpuz80_setValue_ixd(cpu, result);
}

static void cpuz80_res_iyd(struct cpuz80* cpu) {
    /* RES b,(IY+d) */
    uint8_t result = cpuz80_getValue_iyd(cpu);
    CPUZ80_BIT_CLEAR(result, cpu->decoder.ir.xyz.y);
    cpuz80_setValue_iyd(cpu, result);
}

static void cpuz80_jp_nn(struct cpuz80* cpu) {
    /* JP nn */
    cpu->pc = cpuz80_get_imm16(cpu);
}

static void cpuz80_jp_ccnn(struct cpuz80* cpu) {
    /* JP cc,nn */
    const uint16_t newPc = cpuz80_get_imm16(cpu);
    if (cpuz80_getValue_flagCondition(cpu, cpu->decoder.ir.xyz.y)) {
        cpu->pc = newPc;
    }
}

static void cpuz80_jr_e(struct cpuz80* cpu) {
    /* JR e */
    const int8_t offset = cpuz80_get_imm8(cpu);
    cpu->pc += offset;
}

static void cpuz80_jr_cce(struct cpuz80* cpu) {
    /* JR C,e */
    /* JR NC,e */
    /* JR Z,e */
    /* JR NZ,e */
    const int8_t offset = cpuz80_get_imm8(cpu);
    if (cpuz80_getValue_flagCondition(cpu, cpu->decoder.ir.xyz.y - 4)) {
        cpu->pc += offset;
        cpuz80_tick(cpu, 1, 5);
    }
}

static void cpuz80_jp_hl(struct cpuz80* cpu) {
    /* JP (HL) */
    cpu->pc = cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].hl);
}

static void cpuz80_jp_ix(struct cpuz80* cpu) {
    /* JP (IX) */
    cpu->pc = cpu->ix;
}

static void cpuz80_jp_iy(struct cpuz80* cpu) {
    /* JP (IY) */
    cpu->pc = cpu->iy;
}

static void cpuz80_djnz_e(struct cpuz80* cpu) {
    /* DJNZ e */
    const int8_t offset = cpuz80_get_imm8(cpu);
    const uint8_t newB = cpuz80_registerPreDecrement(cpu, 0);
    if (0 != newB) {
        cpu->pc += offset;
        cpuz80_tick(cpu, 1, 5);
    }
}

static void cpuz80_call_nn(struct cpuz80* cpu) {
    /* CALL nn */
    const uint16_t nn = cpuz80_get_imm16(cpu);
    cpuz80_push16(cpu, cpu->pc);
    cpu->pc = nn;
}

static void cpuz80_call_ccnn(struct cpuz80* cpu) {
    /* CALL cc,nn */
    const uint16_t nn = cpuz80_get_imm16(cpu);
    if (cpuz80_getValue_flagCondition(cpu, cpu->decoder.ir.xyz.y)) {
        cpuz80_push16(cpu, cpu->pc);
        cpu->pc = nn;
        cpuz80_tick(cpu, 2, 7);
    }
}

static void cpuz80_ret(struct cpuz80* cpu) {
    /* RET */
    cpu->pc = cpuz80_pop16(cpu);
}

static void cpuz80_ret_cc(struct cpuz80* cpu) {
    /* RET cc */
    if (cpuz80_getValue_flagCondition(cpu, cpu->decoder.ir.xyz.y)) {
        cpu->pc = cpuz80_pop16(cpu);
        cpuz80_tick(cpu, 2, 6);
    }
}

static void cpuz80_reti(struct cpuz80* cpu) {
    /* RETI  */
    cpu->pc = cpuz80_pop16(cpu);
    // TODO potentially inhibit external interrupt line?
}

static void cpuz80_retn(struct cpuz80* cpu) {
    /* RETN  */
    cpu->pc = cpuz80_pop16(cpu);
    cpu->iff1 = cpu->iff2;
}

static void cpuz80_rst(struct cpuz80* cpu) {
    /* RST p  */
    const uint8_t p = 8 * cpu->decoder.ir.xyz.y;
    cpuz80_push16(cpu, cpu->pc);
    cpu->pc = p;
}

static void cpuz80_in_an(struct cpuz80* cpu) {
    /* IN A,(n) */
    const int8_t n = cpuz80_get_imm8(cpu);
    const uint16_t ioAddr = cpuz80_makeWord(
            cpu->afBank[cpu->activeAFBank].af.high,
            n);
    cpu->afBank[cpu->activeAFBank].af.high =
        cpu->l(ioAddr, true, cpu->userData);
}

static void cpuz80_in_rc(struct cpuz80* cpu) {
    /* IN r,(C) */
    const uint16_t ioAddr =
        cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].bc);
    uint8_t value = cpu->l(ioAddr, true, cpu->userData);

    CPUZ80_CLEAR_H(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_CLEAR_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, value == 0);
    CPUZ80_ASSIGN_S(cpu->afBank[cpu->activeAFBank].af.low, value & 0x80);

    if (cpu->decoder.ir.xyz.y == 0x06) {
        cpu->afBank[cpu->activeAFBank].af.low = value;
    } else {
        cpuz80_setValue_register(cpu, cpu->decoder.ir.xyz.y, value);
    }
    CPUZ80_CALC_PV_LOGICAL(cpu->afBank[cpu->activeAFBank].af.low, value);
}

static void cpuz80_ini(struct cpuz80 *cpu) {
    /* INI */
    const uint16_t ioAddr =
        cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].bc);

    const uint8_t ioValue = cpu->l(ioAddr, true, cpu->userData);

    cpu->s(cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].hl),
            false,
            ioValue,
            cpu->userData);

    cpuz80_registerPairPostIncrement(&cpu->bank[cpu->activeBank].hl);
    const uint8_t newB = cpuz80_registerPreDecrement(cpu, 0x00);

    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, newB == 0);
}

static void cpuz80_inir(struct cpuz80 *cpu) {
    /* INIR */
    do {
        const uint16_t ioAddr =
            cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].bc);

        uint8_t ioValue = cpu->l(ioAddr, true, cpu->userData);

        cpu->s(cpuz80_registerPairPostIncrement(&cpu->bank[cpu->activeBank].hl),
                false,
                ioValue,
                cpu->userData);

    } while (cpuz80_registerPreDecrement(cpu, 0x00) != 0);

    if (cpu->decoder.ir.xyz.z == 0x06) {
        cpuz80_tick(cpu, 1, 5);
    }

    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_SET_Z(cpu->afBank[cpu->activeAFBank].af.low);
}

static void cpuz80_ind(struct cpuz80 *cpu) {
    /* IND */
    const uint16_t ioAddr =
        cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].bc);

    const uint8_t ioValue = cpu->l(ioAddr, true, cpu->userData);

    cpu->s(cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].hl),
            false,
            ioValue,
            cpu->userData);

    cpuz80_registerPairPostDecrement(&cpu->bank[cpu->activeBank].hl);
    const uint8_t newB = cpuz80_registerPreDecrement(cpu, 0x00);

    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, newB == 0);
}

static void cpuz80_indr(struct cpuz80 *cpu) {
    /* INDR */
    do {
        const uint16_t ioAddr =
            cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].bc);

        uint8_t ioValue = cpu->l(ioAddr, true, cpu->userData);

        cpu->s(cpuz80_registerPairPostDecrement(&cpu->bank[cpu->activeBank].hl),
                false,
                ioValue,
                cpu->userData);
    } while(cpuz80_registerPreDecrement(cpu, 0x00) != 0x00);

    if (cpu->decoder.ir.xyz.z == 0x06) {
        cpuz80_tick(cpu, 1, 5);
    }

    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_SET_Z(cpu->afBank[cpu->activeAFBank].af.low);
}

static void cpuz80_out_na(struct cpuz80* cpu) {
    /* OUT (n), A */
    const int8_t n = cpuz80_get_imm8(cpu);
    const uint16_t ioAddr = cpuz80_makeWord(
            cpu->afBank[cpu->activeAFBank].af.high,
            n);
    cpu->s(ioAddr, true,
            cpu->afBank[cpu->activeAFBank].af.high, cpu->userData);
}

static void cpuz80_out_cr(struct cpuz80* cpu) {
    /* OUT (C), r */
    const uint16_t ioAddr =
        cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].bc);
    cpu->s(ioAddr, true,
            cpuz80_getValue_register(cpu, cpu->decoder.ir.xyz.y), cpu->userData);
}

static void cpuz80_outi(struct cpuz80 *cpu) {
    /* OUTI */
    const uint8_t memValue = cpu->l(
            cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].hl),
            false,
            cpu->userData);

    const uint8_t newB = cpuz80_registerPreDecrement(cpu, 0x00);

    cpu->s(cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].bc),
            true,
            memValue,
            cpu->userData);

    cpuz80_registerPairPostIncrement(&cpu->bank[cpu->activeBank].hl);
    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, newB == 0);
}

static void cpuz80_otir(struct cpuz80 *cpu) {
    /* OTIR */
    do {
        const uint8_t memValue = cpu->l(
                cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].hl),
                false,
                cpu->userData);

        cpu->s(cpuz80_registerPairPostIncrement(&cpu->bank[cpu->activeBank].hl),
                true,
                memValue,
                cpu->userData);

    } while (cpuz80_registerPreDecrement(cpu, 0x00) != 0x00);

    if (cpu->decoder.ir.xyz.z == 0x06) {
        cpuz80_tick(cpu, 1, 5);
    }

    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_SET_Z(cpu->afBank[cpu->activeAFBank].af.low);
}

static void cpuz80_outd(struct cpuz80 *cpu) {
    /* OUTD */
    const uint8_t memValue = cpu->l(
            cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].hl),
            false,
            cpu->userData);

    const uint8_t newB = cpuz80_registerPreDecrement(cpu, 0x00);

    cpu->s(cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].bc),
            true,
            memValue,
            cpu->userData);

    cpuz80_registerPairPostDecrement(&cpu->bank[cpu->activeBank].hl);
    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_ASSIGN_Z(cpu->afBank[cpu->activeAFBank].af.low, newB == 0);
}

static void cpuz80_otdr(struct cpuz80 *cpu) {
    /* OTDR */
    do {
        const uint8_t memValue = cpu->l(
                cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].hl),
                false,
                cpu->userData);

        cpu->s(cpuz80_registerPairToWord(&cpu->bank[cpu->activeBank].bc),
                true,
                memValue,
                cpu->userData);

        cpuz80_registerPairPostDecrement(&cpu->bank[cpu->activeBank].hl);
    } while (0x00 != cpuz80_registerPreDecrement(cpu, 0x00));

    if (cpu->decoder.ir.xyz.z == 0x06) {
        cpuz80_tick(cpu, 1, 5);
    }

    CPUZ80_SET_N(cpu->afBank[cpu->activeAFBank].af.low);
    CPUZ80_SET_Z(cpu->afBank[cpu->activeAFBank].af.low);
}

static void cpuz80_ignore_prefix(struct cpuz80 *cpu) {
    cpu->pc--;
}

/* ========================================================================== */

static struct cpuz80Instruction cpuz80_decodeXXPrefixes(struct cpuz80* cpu) {
    struct cpuz80Instruction i = {
        .opcode = cpuz80_nop,
        .mCycles = 1,
        .tCycles = 4,
    };

    const uint8_t prior = cpu->decoder.ir.i;
    cpuz80_decoderBuildPrefix(&cpu->decoder);
    cpuz80_decoderFetchInstruction(cpu);
    switch(prior) {
        case 0xcb: i = cpuz80_decodePrefixCb(cpu); break;
        case 0xdd: i = cpuz80_decodePrefixDd(cpu); break;
        case 0xed: i = cpuz80_decodePrefixEd(cpu); break;
        case 0xfd: i = cpuz80_decodePrefixFd(cpu); break;
    }
    return i;
}

static struct cpuz80Instruction cpuz80_decodeDdPrefix(struct cpuz80* cpu) {
    cpuz80_decoderBuildPrefix(&cpu->decoder);
    cpuz80_decoderFetchInstruction(cpu);
    cpu->decoder.d = cpu->decoder.ir.i;
    cpuz80_decoderFetchInstruction(cpu);
    return cpuz80_decodePrefixDdcb(cpu);
}

static struct cpuz80Instruction cpuz80_decodeFdPrefix(struct cpuz80* cpu) {
    cpuz80_decoderBuildPrefix(&cpu->decoder);
    cpuz80_decoderFetchInstruction(cpu);
    cpu->decoder.d = cpu->decoder.ir.i;
    cpuz80_decoderFetchInstruction(cpu);
    return cpuz80_decodePrefixFdcb(cpu);
}

struct cpuz80Instruction cpuz80_decodeUnprefixed(struct cpuz80* cpu) {
    struct cpuz80Instruction i = {
        .opcode = cpuz80_nop,
        .mCycles = 1,
        .tCycles = 4,
    };

    switch (cpu->decoder.ir.i) {
        case 0x00: i = (struct cpuz80Instruction){cpuz80_nop, 1, 4}; break;
        case 0x01: i = (struct cpuz80Instruction){cpuz80_load16_rpn, 2, 10}; break;
        case 0x02: i = (struct cpuz80Instruction){cpuz80_load8_indirecta, 2, 7}; break;
        case 0x03: i = (struct cpuz80Instruction){cpuz80_inc_ss, 1, 6}; break;
        case 0x04: i = (struct cpuz80Instruction){cpuz80_inc_r, 1, 4}; break;
        case 0x05: i = (struct cpuz80Instruction){cpuz80_dec_r, 1, 4}; break;
        case 0x06: i = (struct cpuz80Instruction){cpuz80_load8_rn, 2, 7}; break;
        case 0x07: i = (struct cpuz80Instruction){cpuz80_rlca, 1, 4}; break;
        case 0x08: i = (struct cpuz80Instruction){cpuz80_ex_afaf, 1, 4}; break;
        case 0x09: i = (struct cpuz80Instruction){cpuz80_add16_yxiyhlss, 3, 11}; break;
        case 0x0a: i = (struct cpuz80Instruction){cpuz80_load8_aindirect, 2, 7}; break;
        case 0x0b: i = (struct cpuz80Instruction){cpuz80_dec_ss, 1, 6}; break;
        case 0x0c: i = (struct cpuz80Instruction){cpuz80_inc_r, 1, 4}; break;
        case 0x0d: i = (struct cpuz80Instruction){cpuz80_dec_r, 1, 4}; break;
        case 0x0e: i = (struct cpuz80Instruction){cpuz80_load8_rn, 2, 7}; break;
        case 0x0f: i = (struct cpuz80Instruction){cpuz80_rrca, 1, 4}; break;
        case 0x10: i = (struct cpuz80Instruction){cpuz80_djnz_e, 2, 8}; break;
        case 0x11: i = (struct cpuz80Instruction){cpuz80_load16_rpn, 2, 10}; break;
        case 0x12: i = (struct cpuz80Instruction){cpuz80_load8_indirecta, 2, 7}; break;
        case 0x13: i = (struct cpuz80Instruction){cpuz80_inc_ss, 1, 6}; break;
        case 0x14: i = (struct cpuz80Instruction){cpuz80_inc_r, 1, 4}; break;
        case 0x15: i = (struct cpuz80Instruction){cpuz80_dec_r, 1, 4}; break;
        case 0x16: i = (struct cpuz80Instruction){cpuz80_load8_rn, 2, 7}; break;
        case 0x17: i = (struct cpuz80Instruction){cpuz80_rla, 1, 4}; break;
        case 0x18: i = (struct cpuz80Instruction){cpuz80_jr_e, 3, 12}; break;
        case 0x19: i = (struct cpuz80Instruction){cpuz80_add16_yxiyhlss, 3, 11}; break;
        case 0x1a: i = (struct cpuz80Instruction){cpuz80_load8_aindirect, 2, 7}; break;
        case 0x1b: i = (struct cpuz80Instruction){cpuz80_dec_ss, 1, 6}; break;
        case 0x1c: i = (struct cpuz80Instruction){cpuz80_inc_r, 1, 4}; break;
        case 0x1d: i = (struct cpuz80Instruction){cpuz80_dec_r, 1, 4}; break;
        case 0x1e: i = (struct cpuz80Instruction){cpuz80_load8_rn, 2, 7}; break;
        case 0x1f: i = (struct cpuz80Instruction){cpuz80_rra, 1, 4}; break;
        case 0x20: i = (struct cpuz80Instruction){cpuz80_jr_cce, 1, 4}; break;
        case 0x21: i = (struct cpuz80Instruction){cpuz80_load16_rpn, 2, 10}; break;
        case 0x22: i = (struct cpuz80Instruction){cpuz80_load16_nhl, 5, 16}; break;
        case 0x23: i = (struct cpuz80Instruction){cpuz80_inc_ss, 1, 6}; break;
        case 0x24: i = (struct cpuz80Instruction){cpuz80_inc_r, 1, 4}; break;
        case 0x25: i = (struct cpuz80Instruction){cpuz80_dec_r, 1, 4}; break;
        case 0x26: i = (struct cpuz80Instruction){cpuz80_load8_rn, 2, 7}; break;
        case 0x27: i = (struct cpuz80Instruction){cpuz80_daa, 1, 4}; break;
        case 0x28: i = (struct cpuz80Instruction){cpuz80_jr_cce, 1, 4}; break;
        case 0x29: i = (struct cpuz80Instruction){cpuz80_add16_yxiyhlss, 3, 11}; break;
        case 0x2a: i = (struct cpuz80Instruction){cpuz80_load16_hln, 5, 16}; break;
        case 0x2b: i = (struct cpuz80Instruction){cpuz80_dec_ss, 1, 6}; break;
        case 0x2c: i = (struct cpuz80Instruction){cpuz80_inc_r, 1, 4}; break;
        case 0x2d: i = (struct cpuz80Instruction){cpuz80_dec_r, 1, 4}; break;
        case 0x2e: i = (struct cpuz80Instruction){cpuz80_load8_rn, 2, 7}; break;
        case 0x2f: i = (struct cpuz80Instruction){cpuz80_cpl, 1, 4}; break;
        case 0x30: i = (struct cpuz80Instruction){cpuz80_jr_cce, 1, 4}; break;
        case 0x31: i = (struct cpuz80Instruction){cpuz80_load16_rpn, 2, 10}; break;
        case 0x32: i = (struct cpuz80Instruction){cpuz80_load8_indirecta, 4, 13}; break;
        case 0x33: i = (struct cpuz80Instruction){cpuz80_inc_ss, 1, 6}; break;
        case 0x34: i = (struct cpuz80Instruction){cpuz80_inc_r, 3, 11}; break;
        case 0x35: i = (struct cpuz80Instruction){cpuz80_dec_r, 3, 11}; break;
        case 0x36: i = (struct cpuz80Instruction){cpuz80_load8_rn, 3, 10}; break;
        case 0x37: i = (struct cpuz80Instruction){cpuz80_scf, 1, 4}; break;
        case 0x38: i = (struct cpuz80Instruction){cpuz80_jr_cce, 2, 7}; break;
        case 0x39: i = (struct cpuz80Instruction){cpuz80_add16_yxiyhlss, 3, 11}; break;
        case 0x3a: i = (struct cpuz80Instruction){cpuz80_load8_aindirect, 4, 13}; break;
        case 0x3b: i = (struct cpuz80Instruction){cpuz80_dec_ss, 1, 6}; break;
        case 0x3c: i = (struct cpuz80Instruction){cpuz80_inc_r, 1, 4}; break;
        case 0x3d: i = (struct cpuz80Instruction){cpuz80_dec_r, 1, 4}; break;
        case 0x3e: i = (struct cpuz80Instruction){cpuz80_load8_rn, 2, 7}; break;
        case 0x3f: i = (struct cpuz80Instruction){cpuz80_ccf, 1, 4}; break;
        case 0x40: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x41: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x42: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x43: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x44: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x45: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x46: i = (struct cpuz80Instruction){cpuz80_load8_rr, 2, 7}; break;
        case 0x47: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x48: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x49: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x4a: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x4b: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x4c: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x4d: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x4e: i = (struct cpuz80Instruction){cpuz80_load8_rr, 2, 7}; break;
        case 0x4f: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x50: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x51: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x52: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x53: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x54: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x55: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x56: i = (struct cpuz80Instruction){cpuz80_load8_rr, 2, 7}; break;
        case 0x57: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x58: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x59: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x5a: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x5b: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x5c: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x5d: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x5e: i = (struct cpuz80Instruction){cpuz80_load8_rr, 2, 7}; break;
        case 0x5f: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x60: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x61: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x62: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x63: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x64: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x65: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x66: i = (struct cpuz80Instruction){cpuz80_load8_rr, 2, 7}; break;
        case 0x67: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x68: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x69: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x6a: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x6b: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x6c: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x6d: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x6e: i = (struct cpuz80Instruction){cpuz80_load8_rr, 2, 7}; break;
        case 0x6f: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x70: i = (struct cpuz80Instruction){cpuz80_load8_rr, 2, 7}; break;
        case 0x71: i = (struct cpuz80Instruction){cpuz80_load8_rr, 2, 7}; break;
        case 0x72: i = (struct cpuz80Instruction){cpuz80_load8_rr, 2, 7}; break;
        case 0x73: i = (struct cpuz80Instruction){cpuz80_load8_rr, 2, 7}; break;
        case 0x74: i = (struct cpuz80Instruction){cpuz80_load8_rr, 2, 7}; break;
        case 0x75: i = (struct cpuz80Instruction){cpuz80_load8_rr, 2, 7}; break;
        case 0x76: i = (struct cpuz80Instruction){cpuz80_halt, 1, 4}; break;
        case 0x77: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x78: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x79: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x7a: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x7b: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x7c: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x7d: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x7e: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x7f: i = (struct cpuz80Instruction){cpuz80_load8_rr, 1, 4}; break;
        case 0x80: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x81: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x82: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x83: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x84: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x85: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x86: i = (struct cpuz80Instruction){cpuz80_alu8, 2, 7}; break;
        case 0x87: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x88: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x89: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x8a: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x8b: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x8c: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x8d: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x8e: i = (struct cpuz80Instruction){cpuz80_alu8, 2, 7}; break;
        case 0x8f: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x90: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x91: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x92: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x93: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x94: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x95: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x96: i = (struct cpuz80Instruction){cpuz80_alu8, 2, 7}; break;
        case 0x97: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x98: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x99: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x9a: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x9b: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x9c: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x9d: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x9e: i = (struct cpuz80Instruction){cpuz80_alu8, 2, 7}; break;
        case 0x9f: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xa0: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xa1: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xa2: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xa3: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xa4: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xa5: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xa6: i = (struct cpuz80Instruction){cpuz80_alu8, 2, 7}; break;
        case 0xa7: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xa8: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xa9: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xaa: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xab: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xac: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xad: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xae: i = (struct cpuz80Instruction){cpuz80_alu8, 2, 7}; break;
        case 0xaf: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xb0: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xb1: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xb2: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xb3: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xb4: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xb5: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xb6: i = (struct cpuz80Instruction){cpuz80_alu8, 2, 7}; break;
        case 0xb7: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xb8: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xb9: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xba: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xbb: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xbc: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xbd: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xbe: i = (struct cpuz80Instruction){cpuz80_alu8, 2, 7}; break;
        case 0xbf: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xc0: i = (struct cpuz80Instruction){cpuz80_ret_cc, 1, 5}; break;
        case 0xc1: i = (struct cpuz80Instruction){cpuz80_pop_rp2, 3, 10}; break;
        case 0xc2: i = (struct cpuz80Instruction){cpuz80_jp_ccnn, 3, 10}; break;
        case 0xc3: i = (struct cpuz80Instruction){cpuz80_jp_nn, 3, 10}; break;
        case 0xc4: i = (struct cpuz80Instruction){cpuz80_call_ccnn, 3, 10}; break;
        case 0xc5: i = (struct cpuz80Instruction){cpuz80_push_rp2, 3, 11}; break;
        case 0xc6: i = (struct cpuz80Instruction){cpuz80_alu8, 2, 7}; break;
        case 0xc7: i = (struct cpuz80Instruction){cpuz80_rst, 3, 11}; break;
        case 0xc8: i = (struct cpuz80Instruction){cpuz80_ret_cc, 1, 5}; break;
        case 0xc9: i = (struct cpuz80Instruction){cpuz80_ret, 3, 10}; break;
        case 0xca: i = (struct cpuz80Instruction){cpuz80_jp_ccnn, 3, 10}; break;
        case 0xcc: i = (struct cpuz80Instruction){cpuz80_call_ccnn, 3, 10}; break;
        case 0xcd: i = (struct cpuz80Instruction){cpuz80_call_nn, 5, 17}; break;
        case 0xce: i = (struct cpuz80Instruction){cpuz80_alu8, 2, 7}; break;
        case 0xcf: i = (struct cpuz80Instruction){cpuz80_rst, 3, 11}; break;
        case 0xd0: i = (struct cpuz80Instruction){cpuz80_ret_cc, 1, 5}; break;
        case 0xd1: i = (struct cpuz80Instruction){cpuz80_pop_rp2, 3, 10}; break;
        case 0xd2: i = (struct cpuz80Instruction){cpuz80_jp_ccnn, 3, 10}; break;
        case 0xd3: i = (struct cpuz80Instruction){cpuz80_out_na, 3, 11}; break;
        case 0xd4: i = (struct cpuz80Instruction){cpuz80_call_ccnn, 3, 10}; break;
        case 0xd5: i = (struct cpuz80Instruction){cpuz80_push_rp2, 3, 11}; break;
        case 0xd6: i = (struct cpuz80Instruction){cpuz80_alu8, 2, 7}; break;
        case 0xd7: i = (struct cpuz80Instruction){cpuz80_rst, 3, 11}; break;
        case 0xd8: i = (struct cpuz80Instruction){cpuz80_ret_cc, 1, 5}; break;
        case 0xd9: i = (struct cpuz80Instruction){cpuz80_exx, 1, 4}; break;
        case 0xda: i = (struct cpuz80Instruction){cpuz80_jp_ccnn, 3, 10}; break;
        case 0xdb: i = (struct cpuz80Instruction){cpuz80_in_an, 3, 11}; break;
        case 0xdc: i = (struct cpuz80Instruction){cpuz80_call_ccnn, 3, 10}; break;
        case 0xde: i = (struct cpuz80Instruction){cpuz80_alu8, 2, 7}; break;
        case 0xdf: i = (struct cpuz80Instruction){cpuz80_rst, 3, 11}; break;
        case 0xe0: i = (struct cpuz80Instruction){cpuz80_ret_cc, 1, 5}; break;
        case 0xe1: i = (struct cpuz80Instruction){cpuz80_pop_rp2, 3, 10}; break;
        case 0xe2: i = (struct cpuz80Instruction){cpuz80_jp_ccnn, 3, 10}; break;
        case 0xe3: i = (struct cpuz80Instruction){cpuz80_ex_sphl, 5, 19}; break;
        case 0xe4: i = (struct cpuz80Instruction){cpuz80_call_ccnn, 3, 10}; break;
        case 0xe5: i = (struct cpuz80Instruction){cpuz80_push_rp2, 3, 11}; break;
        case 0xe6: i = (struct cpuz80Instruction){cpuz80_alu8, 2, 7}; break;
        case 0xe7: i = (struct cpuz80Instruction){cpuz80_rst, 3, 11}; break;
        case 0xe8: i = (struct cpuz80Instruction){cpuz80_ret_cc, 1, 5}; break;
        case 0xe9: i = (struct cpuz80Instruction){cpuz80_jp_hl, 1, 4}; break;
        case 0xea: i = (struct cpuz80Instruction){cpuz80_jp_ccnn, 3, 10}; break;
        case 0xeb: i = (struct cpuz80Instruction){cpuz80_ex_dehl, 1, 4}; break;
        case 0xec: i = (struct cpuz80Instruction){cpuz80_call_ccnn, 3, 10}; break;
        case 0xee: i = (struct cpuz80Instruction){cpuz80_alu8, 2, 7}; break;
        case 0xef: i = (struct cpuz80Instruction){cpuz80_rst, 3, 11}; break;
        case 0xf0: i = (struct cpuz80Instruction){cpuz80_ret_cc, 1, 5}; break;
        case 0xf1: i = (struct cpuz80Instruction){cpuz80_pop_rp2, 3, 10}; break;
        case 0xf2: i = (struct cpuz80Instruction){cpuz80_jp_ccnn, 3, 10}; break;
        case 0xf3: i = (struct cpuz80Instruction){cpuz80_di, 1, 4}; break;
        case 0xf4: i = (struct cpuz80Instruction){cpuz80_call_ccnn, 3, 10}; break;
        case 0xf5: i = (struct cpuz80Instruction){cpuz80_push_rp2, 3, 11}; break;
        case 0xf6: i = (struct cpuz80Instruction){cpuz80_alu8, 2, 7}; break;
        case 0xf7: i = (struct cpuz80Instruction){cpuz80_rst, 3, 11}; break;
        case 0xf8: i = (struct cpuz80Instruction){cpuz80_ret_cc, 1, 5}; break;
        case 0xf9: i = (struct cpuz80Instruction){cpuz80_load16_sphl, 1, 6}; break;
        case 0xfa: i = (struct cpuz80Instruction){cpuz80_jp_ccnn, 3, 10}; break;
        case 0xfb: i = (struct cpuz80Instruction){cpuz80_ei, 1, 4}; break;
        case 0xfc: i = (struct cpuz80Instruction){cpuz80_call_ccnn, 3, 10}; break;
        case 0xfe: i = (struct cpuz80Instruction){cpuz80_alu8, 2, 7}; break;
        case 0xff: i = (struct cpuz80Instruction){cpuz80_rst, 3, 11}; break;
        default: i = cpuz80_decodeXXPrefixes(cpu); break;
    }
    return i;
}

/* ========================================================================== */

struct cpuz80Instruction cpuz80_decodePrefixDd(struct cpuz80* cpu) {
    struct cpuz80Instruction i = {
        .opcode = cpuz80_nop,
        .mCycles = 1,
        .tCycles = 4,
    };
    switch (cpu->decoder.ir.i) {
        case 0x09: i = (struct cpuz80Instruction){cpuz80_add16_yxiyhlss, 4, 15}; break;
        case 0x19: i = (struct cpuz80Instruction){cpuz80_add16_yxiyhlss, 4, 15}; break;
        case 0x21: i = (struct cpuz80Instruction){cpuz80_load16_ixn, 4, 14}; break;
        case 0x22: i = (struct cpuz80Instruction){cpuz80_load16_nix, 6, 20}; break;
        case 0x23: i = (struct cpuz80Instruction){cpuz80_inc16_ixiyss, 2, 10}; break;
        case 0x24: i = (struct cpuz80Instruction){cpuz80_inc_p, 1, 4}; break;
        case 0x25: i = (struct cpuz80Instruction){cpuz80_dec_p, 1, 4}; break;
        case 0x26: i = (struct cpuz80Instruction){cpuz80_load8_pn, 1, 4}; break;
        case 0x29: i = (struct cpuz80Instruction){cpuz80_add16_yxiyhlss, 4, 15}; break;
        case 0x2a: i = (struct cpuz80Instruction){cpuz80_load16_indirectixn, 6, 20}; break;
        case 0x2b: i = (struct cpuz80Instruction){cpuz80_dec16_ixiyss, 2, 10}; break;
        case 0x2c: i = (struct cpuz80Instruction){cpuz80_inc_p, 1, 4}; break;
        case 0x2d: i = (struct cpuz80Instruction){cpuz80_dec_p, 1, 4}; break;
        case 0x2e: i = (struct cpuz80Instruction){cpuz80_load8_pn, 1, 4}; break;
        case 0x34: i = (struct cpuz80Instruction){cpuz80_inc_ixd, 6, 23}; break;
        case 0x35: i = (struct cpuz80Instruction){cpuz80_dec_ixd, 6, 23}; break;
        case 0x36: i = (struct cpuz80Instruction){cpuz80_load8_ixdn, 5, 19}; break;
        case 0x39: i = (struct cpuz80Instruction){cpuz80_add16_yxiyhlss, 4, 15}; break;
        case 0x44: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x45: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x46: i = (struct cpuz80Instruction){cpuz80_load8_rixd, 5, 19}; break;
        case 0x4c: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x4d: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x4e: i = (struct cpuz80Instruction){cpuz80_load8_rixd, 5, 19}; break;
        case 0x54: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x55: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x56: i = (struct cpuz80Instruction){cpuz80_load8_rixd, 5, 19}; break;
        case 0x5c: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x5d: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x5e: i = (struct cpuz80Instruction){cpuz80_load8_rixd, 5, 19}; break;
        case 0x60: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x61: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x62: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x63: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x64: i = (struct cpuz80Instruction){cpuz80_nop, 1, 4}; break;
        case 0x65: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x66: i = (struct cpuz80Instruction){cpuz80_load8_rixd, 5, 19}; break;
        case 0x67: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x68: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x69: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x6a: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x6b: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x6c: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x6d: i = (struct cpuz80Instruction){cpuz80_nop, 1, 4}; break;
        case 0x6e: i = (struct cpuz80Instruction){cpuz80_load8_rixd, 5, 19}; break;
        case 0x6f: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x70: i = (struct cpuz80Instruction){cpuz80_load8_ixdr, 5, 19}; break;
        case 0x71: i = (struct cpuz80Instruction){cpuz80_load8_ixdr, 5, 19}; break;
        case 0x72: i = (struct cpuz80Instruction){cpuz80_load8_ixdr, 5, 19}; break;
        case 0x73: i = (struct cpuz80Instruction){cpuz80_load8_ixdr, 5, 19}; break;
        case 0x74: i = (struct cpuz80Instruction){cpuz80_load8_ixdr, 5, 19}; break;
        case 0x75: i = (struct cpuz80Instruction){cpuz80_load8_ixdr, 5, 19}; break;
        case 0x77: i = (struct cpuz80Instruction){cpuz80_load8_ixdr, 5, 19}; break;
        case 0x7c: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x7d: i = (struct cpuz80Instruction){cpuz80_load8_pp, 1, 4}; break;
        case 0x7e: i = (struct cpuz80Instruction){cpuz80_load8_rixd, 5, 19}; break;
        case 0x84: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x85: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x86: i = (struct cpuz80Instruction){cpuz80_alu8, 5, 19}; break;
        case 0x8c: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x8d: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x8e: i = (struct cpuz80Instruction){cpuz80_alu8, 5, 19}; break;
        case 0x94: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x95: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x96: i = (struct cpuz80Instruction){cpuz80_alu8, 5, 19}; break;
        case 0x9c: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x9d: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x9e: i = (struct cpuz80Instruction){cpuz80_alu8, 5, 19}; break;
        case 0xa4: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xa5: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xa6: i = (struct cpuz80Instruction){cpuz80_alu8, 5, 19}; break;
        case 0xac: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xad: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xae: i = (struct cpuz80Instruction){cpuz80_alu8, 5, 19}; break;
        case 0xb4: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xb5: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xb6: i = (struct cpuz80Instruction){cpuz80_alu8, 5, 19}; break;
        case 0xbc: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xbd: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xbe: i = (struct cpuz80Instruction){cpuz80_alu8, 5, 19}; break;
        case 0xcb: i = cpuz80_decodeDdPrefix(cpu); break;
        case 0xe1: i = (struct cpuz80Instruction){cpuz80_pop_rix, 4, 14}; break;
        case 0xe3: i = (struct cpuz80Instruction){cpuz80_ex_spix, 6, 23}; break;
        case 0xe5: i = (struct cpuz80Instruction){cpuz80_push_ix, 4, 15}; break;
        case 0xe9: i = (struct cpuz80Instruction){cpuz80_jp_ix, 2, 8}; break;
        case 0xf9: i = (struct cpuz80Instruction){cpuz80_load16_spix, 2, 10}; break;
        default: i = (struct cpuz80Instruction){cpuz80_ignore_prefix, 1, 4}; break;
    }
    return i;
}

/* ========================================================================== */

struct cpuz80Instruction cpuz80_decodePrefixFd(struct cpuz80* cpu) {
    struct cpuz80Instruction i = {
        .opcode = cpuz80_nop,
        .mCycles = 1,
        .tCycles = 4,
    };
    switch (cpu->decoder.ir.i) {
        case 0x09: i = (struct cpuz80Instruction){cpuz80_add16_yxiyhlss, 4, 15}; break;
        case 0x19: i = (struct cpuz80Instruction){cpuz80_add16_yxiyhlss, 4, 15}; break;
        case 0x21: i = (struct cpuz80Instruction){cpuz80_load16_iyn, 4, 14}; break;
        case 0x22: i = (struct cpuz80Instruction){cpuz80_load16_niy, 6, 20}; break;
        case 0x23: i = (struct cpuz80Instruction){cpuz80_inc16_ixiyss, 2, 10}; break;
        case 0x24: i = (struct cpuz80Instruction){cpuz80_inc_q, 1, 4}; break;
        case 0x25: i = (struct cpuz80Instruction){cpuz80_dec_q, 1, 4}; break;
        case 0x26: i = (struct cpuz80Instruction){cpuz80_load8_qn, 1, 4}; break;
        case 0x29: i = (struct cpuz80Instruction){cpuz80_add16_yxiyhlss, 4, 15}; break;
        case 0x2a: i = (struct cpuz80Instruction){cpuz80_load16_indirectiyn, 6, 20}; break;
        case 0x2b: i = (struct cpuz80Instruction){cpuz80_dec16_ixiyss, 2, 10}; break;
        case 0x2c: i = (struct cpuz80Instruction){cpuz80_inc_q, 1, 4}; break;
        case 0x2d: i = (struct cpuz80Instruction){cpuz80_dec_q, 1, 4}; break;
        case 0x2e: i = (struct cpuz80Instruction){cpuz80_load8_qn, 1, 4}; break;
        case 0x34: i = (struct cpuz80Instruction){cpuz80_inc_iyd, 6, 23}; break;
        case 0x35: i = (struct cpuz80Instruction){cpuz80_dec_iyd, 6, 23}; break;
        case 0x36: i = (struct cpuz80Instruction){cpuz80_load8_iydn, 5, 19}; break;
        case 0x39: i = (struct cpuz80Instruction){cpuz80_add16_yxiyhlss, 4, 15}; break;
        case 0x44: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x45: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x46: i = (struct cpuz80Instruction){cpuz80_load8_riyd, 5, 19}; break;
        case 0x4c: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x4d: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x4e: i = (struct cpuz80Instruction){cpuz80_load8_riyd, 5, 19}; break;
        case 0x54: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x55: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x56: i = (struct cpuz80Instruction){cpuz80_load8_riyd, 5, 19}; break;
        case 0x5c: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x5d: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x5e: i = (struct cpuz80Instruction){cpuz80_load8_riyd, 5, 19}; break;
        case 0x60: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x61: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x62: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x63: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x64: i = (struct cpuz80Instruction){cpuz80_nop, 1, 4}; break;
        case 0x65: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x66: i = (struct cpuz80Instruction){cpuz80_load8_riyd, 5, 19}; break;
        case 0x67: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x68: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x69: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x6a: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x6b: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x6c: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x6d: i = (struct cpuz80Instruction){cpuz80_nop, 1, 4}; break;
        case 0x6e: i = (struct cpuz80Instruction){cpuz80_load8_riyd, 5, 19}; break;
        case 0x6f: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x70: i = (struct cpuz80Instruction){cpuz80_load8_iydr, 5, 19}; break;
        case 0x71: i = (struct cpuz80Instruction){cpuz80_load8_iydr, 5, 19}; break;
        case 0x72: i = (struct cpuz80Instruction){cpuz80_load8_iydr, 5, 19}; break;
        case 0x73: i = (struct cpuz80Instruction){cpuz80_load8_iydr, 5, 19}; break;
        case 0x74: i = (struct cpuz80Instruction){cpuz80_load8_iydr, 5, 19}; break;
        case 0x75: i = (struct cpuz80Instruction){cpuz80_load8_iydr, 5, 19}; break;
        case 0x77: i = (struct cpuz80Instruction){cpuz80_load8_iydr, 5, 19}; break;
        case 0x7c: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x7d: i = (struct cpuz80Instruction){cpuz80_load8_qq, 1, 4}; break;
        case 0x7e: i = (struct cpuz80Instruction){cpuz80_load8_riyd, 5, 19}; break;
        case 0x84: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x85: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x86: i = (struct cpuz80Instruction){cpuz80_alu8, 5, 19}; break;
        case 0x8c: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x8d: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x8e: i = (struct cpuz80Instruction){cpuz80_alu8, 5, 19}; break;
        case 0x94: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x95: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x96: i = (struct cpuz80Instruction){cpuz80_alu8, 5, 19}; break;
        case 0x9c: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x9d: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0x9e: i = (struct cpuz80Instruction){cpuz80_alu8, 5, 19}; break;
        case 0xa4: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xa5: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xa6: i = (struct cpuz80Instruction){cpuz80_alu8, 5, 19}; break;
        case 0xac: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xad: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xae: i = (struct cpuz80Instruction){cpuz80_alu8, 5, 19}; break;
        case 0xb4: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xb5: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xb6: i = (struct cpuz80Instruction){cpuz80_alu8, 5, 19}; break;
        case 0xbc: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xbd: i = (struct cpuz80Instruction){cpuz80_alu8, 1, 4}; break;
        case 0xbe: i = (struct cpuz80Instruction){cpuz80_alu8, 5, 19}; break;
        case 0xcb: i = cpuz80_decodeFdPrefix(cpu); break;
        case 0xe1: i = (struct cpuz80Instruction){cpuz80_pop_riy, 4, 14}; break;
        case 0xe3: i = (struct cpuz80Instruction){cpuz80_ex_spiy, 6, 23}; break;
        case 0xe5: i = (struct cpuz80Instruction){cpuz80_push_iy, 4, 15}; break;
        case 0xe9: i = (struct cpuz80Instruction){cpuz80_jp_iy, 2, 8}; break;
        case 0xf9: i = (struct cpuz80Instruction){cpuz80_load16_spiy, 2, 10}; break;
        default: i = (struct cpuz80Instruction){cpuz80_ignore_prefix, 1, 4}; break;
    }
    return i;
}

/* ========================================================================== */

struct cpuz80Instruction cpuz80_decodePrefixDdcb(struct cpuz80* cpu) {
    struct cpuz80Instruction i = {
        .opcode = cpuz80_nop,
        .mCycles = 1,
        .tCycles = 4,
    };
    switch (cpu->decoder.ir.i) {
        case 0x00: i = (struct cpuz80Instruction){cpuz80_rlc_ixdr, 1, 4}; break;
        case 0x01: i = (struct cpuz80Instruction){cpuz80_rlc_ixdr, 1, 4}; break;
        case 0x02: i = (struct cpuz80Instruction){cpuz80_rlc_ixdr, 1, 4}; break;
        case 0x03: i = (struct cpuz80Instruction){cpuz80_rlc_ixdr, 1, 4}; break;
        case 0x04: i = (struct cpuz80Instruction){cpuz80_rlc_ixdr, 1, 4}; break;
        case 0x05: i = (struct cpuz80Instruction){cpuz80_rlc_ixdr, 1, 4}; break;
        case 0x06: i = (struct cpuz80Instruction){cpuz80_rlc_ixd, 6, 23}; break;
        case 0x07: i = (struct cpuz80Instruction){cpuz80_rlc_ixdr, 1, 4}; break;
        case 0x0e: i = (struct cpuz80Instruction){cpuz80_rrc_ixd, 6, 23}; break;
        case 0x16: i = (struct cpuz80Instruction){cpuz80_rl_ixd, 6, 23}; break;
        case 0x1e: i = (struct cpuz80Instruction){cpuz80_rr_ixd, 6, 23}; break;
        case 0x26: i = (struct cpuz80Instruction){cpuz80_sla_ixd, 6, 23}; break;
        case 0x2e: i = (struct cpuz80Instruction){cpuz80_sra_ixd, 6, 23}; break;
        case 0x36: i = (struct cpuz80Instruction){cpuz80_sll_ixd, 6, 23}; break;
        case 0x3e: i = (struct cpuz80Instruction){cpuz80_srl_ixd, 6, 23}; break;
        case 0x46: i = (struct cpuz80Instruction){cpuz80_bit_ixd, 5, 20}; break;
        case 0x4e: i = (struct cpuz80Instruction){cpuz80_bit_ixd, 5, 20}; break;
        case 0x56: i = (struct cpuz80Instruction){cpuz80_bit_ixd, 5, 20}; break;
        case 0x5e: i = (struct cpuz80Instruction){cpuz80_bit_ixd, 5, 20}; break;
        case 0x66: i = (struct cpuz80Instruction){cpuz80_bit_ixd, 5, 20}; break;
        case 0x6e: i = (struct cpuz80Instruction){cpuz80_bit_ixd, 5, 20}; break;
        case 0x76: i = (struct cpuz80Instruction){cpuz80_bit_ixd, 5, 20}; break;
        case 0x7e: i = (struct cpuz80Instruction){cpuz80_bit_ixd, 5, 20}; break;
        case 0x86: i = (struct cpuz80Instruction){cpuz80_res_ixd, 6, 23}; break;
        case 0x8e: i = (struct cpuz80Instruction){cpuz80_res_ixd, 6, 23}; break;
        case 0x96: i = (struct cpuz80Instruction){cpuz80_res_ixd, 6, 23}; break;
        case 0x9e: i = (struct cpuz80Instruction){cpuz80_res_ixd, 6, 23}; break;
        case 0xa6: i = (struct cpuz80Instruction){cpuz80_res_ixd, 6, 23}; break;
        case 0xae: i = (struct cpuz80Instruction){cpuz80_res_ixd, 6, 23}; break;
        case 0xb6: i = (struct cpuz80Instruction){cpuz80_res_ixd, 6, 23}; break;
        case 0xbe: i = (struct cpuz80Instruction){cpuz80_res_ixd, 6, 23}; break;
        case 0xc6: i = (struct cpuz80Instruction){cpuz80_set_ixd, 6, 23}; break;
        case 0xce: i = (struct cpuz80Instruction){cpuz80_set_ixd, 6, 23}; break;
        case 0xd6: i = (struct cpuz80Instruction){cpuz80_set_ixd, 6, 23}; break;
        case 0xde: i = (struct cpuz80Instruction){cpuz80_set_ixd, 6, 23}; break;
        case 0xe6: i = (struct cpuz80Instruction){cpuz80_set_ixd, 6, 23}; break;
        case 0xee: i = (struct cpuz80Instruction){cpuz80_set_ixd, 6, 23}; break;
        case 0xf6: i = (struct cpuz80Instruction){cpuz80_set_ixd, 6, 23}; break;
        case 0xfe: i = (struct cpuz80Instruction){cpuz80_set_ixd, 6, 23}; break;
        default: break;
    }
    return i;
}

/* ========================================================================== */

struct cpuz80Instruction cpuz80_decodePrefixFdcb(struct cpuz80* cpu) {
    struct cpuz80Instruction i = {
        .opcode = cpuz80_nop,
        .mCycles = 1,
        .tCycles = 4,
    };

    const cpuz80OpCodeT shifts[] = {
        cpuz80_rlc_iyd,
        cpuz80_rrc_iyd,
        cpuz80_rl_iyd,
        cpuz80_rr_iyd,
        cpuz80_sla_iyd,
        cpuz80_sra_iyd,
        cpuz80_sll_iyd,
        cpuz80_srl_iyd,
    };

    switch (cpu->decoder.ir.xyz.x) {
        case 0x00:
            i = (struct cpuz80Instruction){shifts[cpu->decoder.ir.xyz.y], 6, 23};
            break;
        case 0x01:
            i = (struct cpuz80Instruction){cpuz80_bit_iyd, 5, 20};
            break;
        case 0x02:
            i = (struct cpuz80Instruction){cpuz80_res_iyd, 6, 23};
            break;
        case 0x03:
            i = (struct cpuz80Instruction){cpuz80_set_iyd, 6, 23};
            break;

        default:
            break;
    }
    return i;
}

/* ========================================================================== */

struct cpuz80Instruction cpuz80_decodePrefixCb(struct cpuz80* cpu) {
    struct cpuz80Instruction i = {
        .opcode = cpuz80_nop,
        .mCycles = 1,
        .tCycles = 4,
    };

    const cpuz80OpCodeT shifts[] = {
        cpuz80_rlc_r,
        cpuz80_rrc_r,
        cpuz80_rl_r,
        cpuz80_rr_r,
        cpuz80_sla_r,
        cpuz80_sra_r,
        cpuz80_sll_r,
        cpuz80_srl_r,
    };

    switch (cpu->decoder.ir.xyz.x) {
        case 0x00:
            i = (struct cpuz80Instruction){shifts[cpu->decoder.ir.xyz.y], 2, 8};
            break;

        case 0x01:
            i = (struct cpuz80Instruction){cpuz80_bit_r, 2, 8};
            break;

        case 0x02:
            i = (struct cpuz80Instruction){cpuz80_res_r, 2, 8};
            break;

        case 0x03:
            i = (struct cpuz80Instruction){cpuz80_set_r, 2, 8};
            break;

        default:
            break;
    }

    return i;
}

/* ========================================================================== */

struct cpuz80Instruction cpuz80_decodePrefixEd(struct cpuz80* cpu) {
    struct cpuz80Instruction i = {
        .opcode = cpuz80_nop,
        .mCycles = 1,
        .tCycles = 4,
    };
    switch (cpu->decoder.ir.i) {
        case 0x40: i = (struct cpuz80Instruction){cpuz80_in_rc, 3, 12}; break;
        case 0x41: i = (struct cpuz80Instruction){cpuz80_out_cr, 3, 12}; break;
        case 0x42: i = (struct cpuz80Instruction){cpuz80_sbc16_hlss, 4, 15}; break;
        case 0x43: i = (struct cpuz80Instruction){cpuz80_load16_nhl, 6, 20}; break;
        case 0x44: i = (struct cpuz80Instruction){cpuz80_neg, 2, 8}; break;
        case 0x45: i = (struct cpuz80Instruction){cpuz80_retn, 1, 4}; break;
        case 0x46: i = (struct cpuz80Instruction){cpuz80_im0, 2, 8}; break;
        case 0x47: i = (struct cpuz80Instruction){cpuz80_load8_ia, 2, 9}; break;
        case 0x48: i = (struct cpuz80Instruction){cpuz80_in_rc, 3, 12}; break;
        case 0x49: i = (struct cpuz80Instruction){cpuz80_out_cr, 3, 12}; break;
        case 0x4a: i = (struct cpuz80Instruction){cpuz80_adc16_hlss, 4, 15}; break;
        case 0x4b: i = (struct cpuz80Instruction){cpuz80_load16_indirectrpn, 6, 20}; break;
        case 0x4c: i = (struct cpuz80Instruction){cpuz80_neg, 2, 8}; break;
        case 0x4d: i = (struct cpuz80Instruction){cpuz80_reti, 4, 14}; break;
        case 0x4e: i = (struct cpuz80Instruction){cpuz80_im0, 2, 8}; break;
        case 0x4f: i = (struct cpuz80Instruction){cpuz80_load8_ra, 2, 9}; break;
        case 0x50: i = (struct cpuz80Instruction){cpuz80_in_rc, 3, 12}; break;
        case 0x51: i = (struct cpuz80Instruction){cpuz80_out_cr, 3, 12}; break;
        case 0x52: i = (struct cpuz80Instruction){cpuz80_sbc16_hlss, 4, 15}; break;
        case 0x53: i = (struct cpuz80Instruction){cpuz80_load16_nhl, 6, 20}; break;
        case 0x54: i = (struct cpuz80Instruction){cpuz80_neg, 2, 8}; break;
        case 0x55: i = (struct cpuz80Instruction){cpuz80_retn, 1, 14}; break;
        case 0x56: i = (struct cpuz80Instruction){cpuz80_im1, 2, 8}; break;
        case 0x57: i = (struct cpuz80Instruction){cpuz80_load8_ai, 2, 9}; break;
        case 0x58: i = (struct cpuz80Instruction){cpuz80_in_rc, 3, 12}; break;
        case 0x59: i = (struct cpuz80Instruction){cpuz80_out_cr, 3, 12}; break;
        case 0x5a: i = (struct cpuz80Instruction){cpuz80_adc16_hlss, 4, 15}; break;
        case 0x5b: i = (struct cpuz80Instruction){cpuz80_load16_indirectrpn, 6, 20}; break;
        case 0x5c: i = (struct cpuz80Instruction){cpuz80_neg, 2, 8}; break;
        case 0x5d: i = (struct cpuz80Instruction){cpuz80_retn, 1, 4}; break;
        case 0x5e: i = (struct cpuz80Instruction){cpuz80_im2, 2, 8}; break;
        case 0x5f: i = (struct cpuz80Instruction){cpuz80_load8_ar, 2, 9}; break;
        case 0x60: i = (struct cpuz80Instruction){cpuz80_in_rc, 3, 12}; break;
        case 0x61: i = (struct cpuz80Instruction){cpuz80_out_cr, 3, 12}; break;
        case 0x62: i = (struct cpuz80Instruction){cpuz80_sbc16_hlss, 4, 15}; break;
        case 0x63: i = (struct cpuz80Instruction){cpuz80_load16_nhl, 6, 20}; break;
        case 0x64: i = (struct cpuz80Instruction){cpuz80_neg, 2, 8}; break;
        case 0x65: i = (struct cpuz80Instruction){cpuz80_retn, 1, 4}; break;
        case 0x66: i = (struct cpuz80Instruction){cpuz80_im0, 2, 8}; break;
        case 0x67: i = (struct cpuz80Instruction){cpuz80_rrd, 5, 18}; break;
        case 0x68: i = (struct cpuz80Instruction){cpuz80_in_rc, 3, 12}; break;
        case 0x69: i = (struct cpuz80Instruction){cpuz80_out_cr, 3, 12}; break;
        case 0x6a: i = (struct cpuz80Instruction){cpuz80_adc16_hlss, 4, 15}; break;
        case 0x6b: i = (struct cpuz80Instruction){cpuz80_load16_indirectrpn, 6, 20}; break;
        case 0x6c: i = (struct cpuz80Instruction){cpuz80_neg, 2, 8}; break;
        case 0x6d: i = (struct cpuz80Instruction){cpuz80_retn, 1, 4}; break;
        case 0x6e: i = (struct cpuz80Instruction){cpuz80_im0, 2, 8}; break;
        case 0x6f: i = (struct cpuz80Instruction){cpuz80_rld, 5, 18}; break;
        case 0x70: i = (struct cpuz80Instruction){cpuz80_in_rc, 3, 12}; break;
        case 0x71: i = (struct cpuz80Instruction){cpuz80_out_cr, 3, 12}; break;
        case 0x72: i = (struct cpuz80Instruction){cpuz80_sbc16_hlss, 4, 15}; break;
        case 0x73: i = (struct cpuz80Instruction){cpuz80_load16_nhl, 6, 20}; break;
        case 0x74: i = (struct cpuz80Instruction){cpuz80_neg, 2, 8}; break;
        case 0x75: i = (struct cpuz80Instruction){cpuz80_retn, 1, 4}; break;
        case 0x76: i = (struct cpuz80Instruction){cpuz80_im1, 2, 8}; break;
        case 0x78: i = (struct cpuz80Instruction){cpuz80_in_rc, 3, 12}; break;
        case 0x79: i = (struct cpuz80Instruction){cpuz80_out_cr, 3, 12}; break;
        case 0x7a: i = (struct cpuz80Instruction){cpuz80_adc16_hlss, 4, 15}; break;
        case 0x7b: i = (struct cpuz80Instruction){cpuz80_load16_indirectrpn, 6, 20}; break;
        case 0x7c: i = (struct cpuz80Instruction){cpuz80_neg, 2, 8}; break;
        case 0x7d: i = (struct cpuz80Instruction){cpuz80_retn, 1, 4}; break;
        case 0x7e: i = (struct cpuz80Instruction){cpuz80_im2, 2, 8}; break;
        case 0xa0: i = (struct cpuz80Instruction){cpuz80_ldi, 4, 16}; break;
        case 0xa1: i = (struct cpuz80Instruction){cpuz80_cpi, 4, 16}; break;
        case 0xa2: i = (struct cpuz80Instruction){cpuz80_ini, 4, 16}; break;
        case 0xa3: i = (struct cpuz80Instruction){cpuz80_outi, 4, 16}; break;
        case 0xa8: i = (struct cpuz80Instruction){cpuz80_ldd, 4, 16}; break;
        case 0xa9: i = (struct cpuz80Instruction){cpuz80_cpd, 4, 16}; break;
        case 0xaa: i = (struct cpuz80Instruction){cpuz80_ind, 4, 16}; break;
        case 0xab: i = (struct cpuz80Instruction){cpuz80_outd, 4, 16}; break;
        case 0xb0: i = (struct cpuz80Instruction){cpuz80_ldir, 4, 16}; break;
        case 0xb1: i = (struct cpuz80Instruction){cpuz80_cpir, 4, 16}; break;
        case 0xb2: i = (struct cpuz80Instruction){cpuz80_inir, 4, 16}; break;
        case 0xb3: i = (struct cpuz80Instruction){cpuz80_otir, 4, 16}; break;
        case 0xb8: i = (struct cpuz80Instruction){cpuz80_lddr, 4, 16}; break;
        case 0xb9: i = (struct cpuz80Instruction){cpuz80_cpdr, 4, 16}; break;
        case 0xba: i = (struct cpuz80Instruction){cpuz80_indr, 4, 16}; break;
        case 0xbb: i = (struct cpuz80Instruction){cpuz80_otdr, 4, 16}; break;
        default: i = (struct cpuz80Instruction){cpuz80_ignore_prefix, 1, 4}; break;
    }
    return i;
}

/* ========================================================================== */
